#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script contains the main functions for the computations such as loading data,
binarized data, computing correlations, mutual information and Kulback-Leibler divergence

Created on Mon Jun  3 12:09:29 2019

@author: susan.ghaderi

"""

import numpy as np
import math
import csv
import matplotlib.pyplot as plt
from scipy.stats import chi2
from scipy.stats.stats import pearsonr
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from scipy.stats import norm
from scipy.stats import uniform
from scipy.stats import entropy
 
#-------------------------- extract data from txt file ------------------------

def My_get_from_txt(name):
    """
    reads data from text files delivered by Drop_Seq bioinf pipline
    first column contains gene names
    
    Input: name=file
    Output: gene list, expression matrix in list for,
    expression matrix in numpy format
    """
    f = open(name)
    TEMP = f.readlines()
    f.close()
    fullPrhase = TEMP[0]
    temp = TEMP[1:] #[0]
    
    geneNames = fullPrhase.split()
    gn =[]
    for line in geneNames:
        gn.append(line.replace('"',''))
    geneNames = gn    

    cellIdentifiers = []
    expressionValues = []
    for line in temp:
        splittedTemp = line.split()
        cellIdentifiers.append(splittedTemp[0:4])
        thisLineList = splittedTemp[4:]
        newLineList = []
        for element in thisLineList:
            numericalElement = float(element)
            newLineList.append(numericalElement)
        expressionValues.append(newLineList)
    return geneNames, cellIdentifiers, expressionValues

#------------------------------binarization of the matrix ---------------------

def get_On_Off_binary(M):
    """
    Simple function that performs binarization based on 
    the ON/OFF of gene expression
    
    Input: matrix where rows are genes, columns represent cells
    Output: corresponding matrix with 0/1 expressions
    """
    M = np.array(M)
    M_bin = np.empty([np.shape(M)[0],np.shape(M)[1]])
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            M_bin[i,j] = 1 if M[i,j]>0 else 0
    return np.array(M_bin)
#------------------------------ divide the data set to 10 data set
def get_cell_matrix(cellIdentifiers, expressionValues, cellName):
    """ This function put the data as a gene-cell Matrix array
    """
    cellMatrix = []
    for j in cellIdentifiers:
        d = j[0]
        if cellName in d:
           t = cellIdentifiers.index(j)
           r = expressionValues[t]
           cellMatrix.append(r)
          
    return np.transpose(cellMatrix)  
#------------ binaraization with median of whole matrix expression ------------
def get_binary_median_2(M, E):
    """
    Simple function that performs binarization based on the median
    of the total gene expression matrix
    
    Input: M: matrix where rows are genes,
           columns represent cells
           E: total expression matrix where 
           rows are genes, columns represent cells
    Output: corresponding matrix with 0/1 expressions
    """
    M = np.array(M)
    E = np.array(E)
    aux = np.median(E,axis=1)
    M2_bin = np.empty([np.shape(M)[0],np.shape(M)[1]])
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            M2_bin[i,j] = 1 if M[i,j]>aux[i] else 0
    return np.array(M2_bin)
#---------- binaraization with median of one cell matrix expression -----------
def get_binary_median_3(M):
    """
    Simple function that performs binarization 
    based on the median of the sample gene expression
    
    Input: matrix where rows are genes, columns represent cells
    Output: corresponding matrix with 0/1 expressions
    """
    M = np.array(M)
    aux = np.median(M,axis=1)#np.mean(M,axis=1)#
    M_bin = np.empty([np.shape(M)[0],np.shape(M)[1]])
    for j in range(np.shape(M)[1]):
        for i in range(np.shape(M)[0]):
            M_bin[i,j] = 1 if M[i,j]>aux[i] else 0
    return np.array(M_bin)



#--------------------Dimensionality reduction (pca, tsne ______________________
def get_DR(data_matrix, g_ind=-1, d=2, off=[], t1='PCA', t2='tSNE', SaveAs1='PCA', SaveAs2='tSNE'):
    """
    Dimensionality reduction (pca, tsne, 2 or 3 dimensions)
    g_ind: index of gene used to color plots
    d: dimensions; off: indices of excluded genes (list)
    t1, t2: titles of pca and tsna plots
    """
    M = np.copy(data_matrix)
   # cm = np.zeros(np.shape(M[0,:])) if g_ind==-1 else M[g_ind,:]
   # print(cm)
    cm =['red','grey']
    M[off,:] = 0
    tsne = TSNE(n_components=d, random_state=0)   
    y_tsne = tsne.fit_transform(np.transpose(M))  
    pca = PCA(n_components=d)
    y_pca = pca.fit_transform(np.transpose(M))
    if d == 2:
        plt.figure()
        plt.scatter(y_pca[:,0],y_pca[:,1], c=cm)
        plt.colorbar()
        plt.title(t1)
        plt.savefig(SaveAs1)
        plt.figure()
        plt.scatter(y_tsne[:,0],y_tsne[:,1], c=cm)
        plt.title(t2)
        plt.colorbar()
        plt.savefig(SaveAs2)
    elif d == 3:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(y_pca[:,0], y_pca[:,1], y_pca[:,2], c=cm)
        #plt.colorbar(mappable=cm)
        plt.title(t1)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(y_tsne[:,0], y_tsne[:,1], y_tsne[:,2], c=cm)
        #ax.colorbar()
        plt.title(t2)
    return y_pca, y_tsne
####---------------------------- Correlation Matrix ---------------------------
def get_corr(M, g):
    """It computes correlation between cells or genes
    """
    out = np.empty(np.shape(M)[0])
    out1 = np.zeros(shape=(np.shape(M)[0],np.shape(M)[0]))
    n =np.shape(M)[0]
    N= n*n
    for p in range(np.shape(M)[0]):
        print(f'p={p}')
        for i in range(np.shape(M)[0]):
            PearsonCorrCoeff, pval = pearsonr(M[p,:], M[i,:])
            aux = PearsonCorrCoeff
            if g==1:
                if pval < (0.01)/N:
                    aux = aux
                else: 
                  aux = 0
                out[i] = 0 if np.isnan(aux) else aux   
            else:      
                out[i] = 0 if np.isnan(aux) else aux    
        out1[p] = out 

    return out1
###----------------------------------------------


def get_probability(r1, r2=None):
    """
    This function calculates the probability distribution of a vector of symbols.
    Input: list or string; or two lists r strings.(the need to be of the same length)
    Output: In case of a single input, we get probability distribution. 
    In case of two inputs, we get join distribution and marginal distribution.
    A distribution is a dictionary object. To see how it works try the following:
        P = get_probability('aaabbb')
        P
        P['a']
    """
    r1 = r1.tolist() if type(r1)==np.ndarray else r1 
    r2 = r2.tolist() if type(r2)==np.ndarray else r2 
    aux1 = set(r1)
    p1 = []
    for i in aux1:
        p1.append([i, r1.count(i)/len(r1)])
    p1 = dict(p1)    
    if r2!=None:
        p2 = []
        aux2 = set(r2)
        for i in aux2:
            p2.append([i, r2.count(i)/len(r2)])
    else:
        p2 = None 
    if r2 != None:
        aux12 = []
        for i in aux1:
            for j in aux2:
                aux12.append((i, j))
        mer = []         
        for i, iaux in enumerate(r1):         
            mer.append((r1[i],r2[i]))
        p12 = []
        for i in aux12:
            p12.append([i, mer.count(i)/len(r1)])
    else:
        p12 = None
    p2 = dict(p2) if p2!=None else None
    p12 = dict(p12) if p12!=None else None
    if r2 == None:
        return p1
    else:
        return p1, p2, p12




###---------------------------------------------------------------------------- 

###-------- 
def MI_2(M,g):
        """
        Mutual information (MI_2) between gene-gene and cell-cell
        M: is a gene by cell matrix
        OUTPUT: MI score, and p-value from G test, see wikipedia
        """
        n = np.shape(M)[0]
    #    n1 = np.shape(M)[1]
        col1 = np.zeros(n)
        col2 = np.zeros(n)
        MI_Matrix = np.zeros(shape=(n,n))
        pv_Matrix = np.zeros(shape=(n,n))
        for p in range(n):
            for t in range(n):
              #  d = n1*[0]+n1*[1]               
                a = M[p]
                b = M[t]
                a = a.tolist() if type(a)==np.ndarray else a 
                b = b.tolist() if type(b)==np.ndarray else b 
                a,b,c = get_probability(a,b)#(d,g)
                aux1 = 0
                for i,j in c.items():
                    aux1 = aux1 + j*math.log(j,2) if j>0 else aux1
                aux2 = 0
                for i,j in a.items():
                    for k,l in b.items():
                        aux2 = aux2 + j*l*math.log(j*l,2) if j*l>0 else aux2
                mi = (aux1 - aux2)
                col1[t]=mi
                aux = mi/math.log(math.exp(1),2)#change the base for the G-test 
                gt = 1-chi2.cdf(2*len(a)*aux,((len(a)-1)*(len(b)-1)))
                col2[t] = gt
                if g ==1:
                    if gt<(0.01/n*n):
                        mi =mi
                    else:
                        mi=0
                    col1[t]=mi    
            MI_Matrix[p] =col1
            pv_Matrix[p] =col2 
    
        return MI_Matrix#, pv_Matrix#, MI_Matirx

   
    
def save_array(X):
    A = np.array(X)
    c = 0
    csv.register_dialect(
        'mydialect',
        delimiter = ';',
        quotechar = '"',
        doublequote = True,
        skipinitialspace = True,
        lineterminator = '\r\n',
        quoting = csv.QUOTE_MINIMAL)
    with open('mydata.csv', 'w') as mycsvfile:
        thedatawriter = csv.writer(mycsvfile, dialect='mydialect')
        for row in A:
            thedatawriter.writerow(row)
            c = c+1
    return c    
###--------------------------------
def merge(A,B):
    """
    Merge two matrices A and B as [A,B]
    """
    M = []
    n = np.shape(A)[0]
    for i in range(n):
        a = A[i]
        b = B[i]
        a = a.tolist() if type(a)==np.ndarray else a 
        b = b.tolist() if type(b)==np.ndarray else b 
        M.append(a+b)
    return np.array(M)

###___________________________________


def avg(M):
    """
    This method calculates stats for all genes
    """
    M = abs(M)
    mean_x = []
    ave = np.mean(M)# np.triu(M, k=1))
    for i in range(np.shape(M)[0]):
        a = M[i]
        mean_x.append(np.mean(a))
    return ave, mean_x 
 
   
       
def get_SEM(A,B,C,D): # WE ALSO CHANGED THE INPUTS
    """A is average gene-gene correlation
       B is average cell-cell correlation
       C is error for average gg correlation 
       D is error for average cc correlation 
    """
    A = np.array(A)
    SEM = np.zeros(np.shape(A)[0])
    for i in range(np.shape(A)[0]):# x,y,z,w in A,B,C,D:
        print(i)
        x = np.array(A[i])
        y = np.array(B[i])
        Error_gg = np.array(C[i])
        Error_cc = np.array(D[i])
        s=math.sqrt(((1/y)**2)*(Error_gg**2)+(((-x/y**2)**2)*Error_cc**2))
        SEM[i]=s
    return SEM



#####------------------------
def get_probability2(r1, r2=None):
    """
    This function get the probability distribution of the binary vectors a and b
    """
    r1 = r1.tolist() if type(r1)==np.ndarray else r1 
    r2 = r2.tolist() if type(r2)==np.ndarray else r2 
    aux1 = set(r1)
    p1 = []
    for i in aux1:
        p1.append([i,r1.count(i)/len(r1)])
    p1 = dict(p1)
    if r2!=None:
        p2 = []
        aux2 = set(r2)
        for i in aux2:
            p2.append([i, r2.count(i)/len(r2)])
        p2 = dict(p2)
    else:
        p2 = None       
    t1=np.zeros(np.shape(r1)[0])
    for i in range(np.shape(r1)[0]):
        for j, k in p1.items():
            if r1[i]== j:
               t1[i]=p1[j]
    if r2!=None:           
        t2=np.zeros(np.shape(r2)[0])
        for i in range(np.shape(r2)[0]):
            for j, k in p2.items():
                if r2[i]== j:
                   t2[i]=p2[j]   
    t1 = np.array(t1) if r1!=None else None
    t2 = np.array(t2) if r2!=None else None

    if r2 == None:
        return t1
    else:
        return t1, t2
 



def gibbs(gene, geneNames, M1, M2, prob, sym = False):
    
    a = M1[geneNames.index(gene)]
    b = M2[geneNames.index(gene)]
    if prob == 'binary':
        if np.any(b) and ((max(b)-min(b))!=0):
            p = get_probability2(b)
        else:
            p=len(b)*[1] 
        if np.any(a) and ((max(a)-min(a))!=0):
            x,y = uniform.fit(a)   
            q = uniform.pdf(a,x,y)
           # q = get_probability2(a)
        else:
            q=len(p)*[1]
                                
    if prob =='Normal':
        p = b
        q = uniform.rvs(size=np.shape(p)[0])
            
    # I add this section on 16.09.2020 for blood cell data, 
    #because the number of genes is small, and randomness dominate 
    #the results, therefore, I use discrete uniform distribution           
    if prob == 'bc':
        if np.any(b) and ((max(b)-min(b))!=0):
           mu, std = norm.fit(b)
           p = norm.pdf(sorted(b), mu, std)
        else:
            p=len(b)*[1] 
  
        q =np.array(len(b)*[1/len(b)])   
        
    if np.shape(q)[0] != np.shape(p)[0]:
        np.random.seed(1234)
        p = np.random.choice(p,np.shape(q)[0])
        
    """
    Compute KL values
    """        
    if (p.any())==False:
        KL = 0
    else:    
        KL = entropy(p,q)

   # print(KL)
    return KL
            
