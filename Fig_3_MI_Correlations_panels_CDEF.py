"""
Created on Mon Feb 11 18:26:18 2019

@author: susan.ghaderi
In this script, we compute the the correlation, and we plot average cell-cell, 
gene-gene correlation and the correlation based transition indices for both 
IPS cell and blood cell data (Subfigures 3C and 3D).
A heatmap is plotted of the correlation matrix for the DEGs (Subfigure 3A and 3B).
Furhtermore, it binarizes data based on median binarization or on-off binarization and  
compute cell-cell, gene-gene MI avarages and the MI-based transition indices (Subfigures 
3E & 3F). The input data are the mutant and control cell iPSC data.
"""
import numpy as np
from Criticl_index_computation import CriticalIndex
import General_Functions as GF
import os




load_Data =0 
get_data = 1


load_Data = 0 
get_data = 0

Correlation = 0
Mutual_Info = 0
plotFig = 0
Gibbs_energy = 0
CG = 0

#%%
if load_Data ==1:
    """
    Load data that is already quality filtered and saved in folder data.
    """
    A_cut_cell  = np.load(os.path.join('data/IPS_data', 'A_cut_cell.npy'), allow_pickle=True)
    B_cut_cell  = np.load(os.path.join('data/IPS_data', 'B_cut_cell.npy'), allow_pickle=True)
    A_geneNames = np.load(os.path.join('data/IPS_data', 'A_geneNames.npy'), allow_pickle=True)
    B_geneNames = np.load(os.path.join('data/IPS_data', 'B_geneNames.npy'), allow_pickle=True)
#%%    
if get_data == 1:
    
    """
    To use the CriticalIndex class, first an objact class for each 
    cell type should be generated. For example one object for A and on for B, e.g.,
    A_ob and B_obj.
    The input for each object is the cell expression matrix and the corrsponding gene names.
    """       
    A_obj = CriticalIndex(A_cut_cell, A_geneNames) 
    
    """ Determination of common gene names between A and B """
    common_genes = A_obj.com_genes(A_geneNames, B_geneNames)
   
    """Final data based on common genes"""
    A_data_final = A_obj.final_data()
    
    """ Normalization of data"""
    A_data_norm = A_obj.normalized_data(0)
    
    
    
    B_obj = CriticalIndex(B_cut_cell, B_geneNames) 
    
    """ Common gene names between A and B """
    common_genes = B_obj.com_genes(B_geneNames, A_geneNames)
   
    """Final data based on common genes"""
    B_data_final = B_obj.final_data()
    
    """ Normalized of data"""
    B_data_norm = B_obj.normalized_data(0)
    

    
    """ Binarization of data with on/off and median approach"""
    data_binary_1_A, data_binary_2_A = A_obj.binarization()
    data_binary_1_B, data_binary_2_B = B_obj.binarization()
    
#%%    
if Correlation == 1: 
    """
    Computation of the correlation matrices and ubsequent calculations of the indices and error bars.
    The input for the correlation function: Correlation(g = 0, name = None, data_name =None,
                                                        g_samples = None, c_samples = None,
                                                        bootsNumbers = None, BS = False, 
                                                        save = False) are:
        g           : filter data by p-value or not, 0 or 1
        name        : Mutant(A) or control(B)
        data name   : the name that you would like to save data later
        g_samples   : the number of genes you would like to bootsrtap, we consider it 200 out of 16662 
        c_samples   : the number of cells you would like to bootsrtap, we consider it 50 outof 250
        bootsNumbers: The number of bootstraping iteration
        BS          : if youwould like to bootstrap or not, the default is false
        save: if you would like to save the results or no
    """
    print("Starting correlation calculation")
    A_data_gg_cor_avg, A_data_cc_cor_avg = A_obj.Correlation(1, 'A', 'Jonas_pv001', 200, 50, 200, True, True)
    B_data_gg_cor_avg, B_data_cc_cor_avg = B_obj.Correlation(1, 'B', 'Jonas_pv001', 200, 50, 200, True, True)

    A_gg_cor_avg, A_cc_cor_avg, A_idx_cor = A_obj.Index(A_data_gg_cor_avg, A_data_cc_cor_avg, True)
    EB_A_gg_cor, EB_A_cc_cor, EB_A_idx_cor = A_obj.EB_SEM(A_gg_cor_avg, A_cc_cor_avg, A_data_gg_cor_avg, A_data_cc_cor_avg, True)

    B_gg_cor_avg, B_cc_cor_avg, B_idx_cor = B_obj.Index(B_data_gg_cor_avg, B_data_cc_cor_avg, True)
    EB_B_gg_cor, EB_B_cc_cor, EB_B_idx_cor = B_obj.EB_SEM(B_gg_cor_avg, B_cc_cor_avg, B_data_gg_cor_avg, B_data_cc_cor_avg, True)


#%%
""" Plotting heat map of gene-gene and cell-cell correlation """


#%%
if Mutual_Info == 1:
    """
    Using Mutual information (MI) computes average gene-gene  
    MI, average cell-cell  MI and finally gets critial MI indices for each time
    point.
    The inputs of Mutual_information function (
        Mutual_information(g = 0, binary_type ='on_off', name = None,
                           data_name =None, g_samples = None, c_samples = None,
                           bootsNumbers = None, BS = False, save = False):) are:
        g           : filter data by p-value or not, 0 or 1 
        binary_type : it can be on-off binary or median binary
        name        : Mutant(A) or control(B)
        data name   : the name that you would like to save data later
        g_samples   : the number of genes you would like to bootsrtap, we consider it 200 out of 16662 
        c_samples   : the number of cells you would like to bootsrtap, we consider it 40 outof 250
        bootsNumbers: The number of bootstraping iteration
        BS          : if youwould like to bootstrap or not, the default is false
        save: if you would like to save the results or no        
    """

    
    A_data_gg_MI_avg_1, A_data_cc_MI_avg_1 = A_obj.Mutual_information(0, 'on_off', 'A', 'Jonas', 200, 40, 500, True, True)
    A_gg_MI_avg_1, A_cc_MI_avg_1, A_idx_MI_1 = A_obj.Index(A_data_gg_MI_avg_1, A_data_cc_MI_avg_1)
    EB_A_gg_MI_1, EB_A_cc_MI_1, EB_A_idx_MI_1 = A_obj.EB_SEM(A_gg_MI_avg_1, A_cc_MI_avg_1, A_data_gg_MI_avg_1, A_data_cc_MI_avg_1, 200)# A_data_EB_gg_MI_1, A_data_EB_cc_MI_1)
#    
    A_data_gg_MI_avg_2, A_data_cc_MI_avg_2 = A_obj.Mutual_information(0, 'median_all', 'A', 'Jonas', 200, 40, 500, True, True)
    A_gg_MI_avg_2, A_cc_MI_avg_2, A_idx_MI_2 = A_obj.Index(A_data_gg_MI_avg_2, A_data_cc_MI_avg_2)
    EB_A_gg_MI_2, EB_A_cc_MI_2, EB_A_idx_MI_2 = A_obj.EB_SEM(A_gg_MI_avg_2, A_cc_MI_avg_2, A_data_gg_MI_avg_2, A_data_cc_MI_avg_2, 200)
    
    B_data_gg_MI_avg_1, B_data_cc_MI_avg_1 = B_obj.Mutual_information(0, 'on_off', 'B', 'Jonas', 200, 40, 40, True, True)
    B_gg_MI_avg_1, B_cc_MI_avg_1, B_idx_MI_1 = B_obj.Index(B_data_gg_MI_avg_1, B_data_cc_MI_avg_1)
    EB_B_gg_MI_1, EB_B_cc_MI_1, EB_B_idx_MI_1 = B_obj.EB_SEM(B_gg_MI_avg_1, B_cc_MI_avg_1, B_data_gg_MI_avg_1, B_data_cc_MI_avg_1, 200)# B_data_EB_gg_MI_1, B_data_EB_cc_MI_1)
#    
    B_data_gg_MI_avg_2, B_data_cc_MI_avg_2 = B_obj.Mutual_information(0, 'median_all', 'B', 'Jonas', 200, 40, 500, True, True)
    B_gg_MI_avg_2, B_cc_MI_avg_2, B_idx_MI_2 = B_obj.Index(B_data_gg_MI_avg_2, B_data_cc_MI_avg_2)
    EB_B_gg_MI_2, EB_B_cc_MI_2, EB_B_idx_MI_2 = B_obj.EB_SEM(B_gg_MI_avg_2, B_cc_MI_avg_2, B_data_gg_MI_avg_2, B_data_cc_MI_avg_2, 200)
        
if plotFig == 1:
    
    
    A_obj.Plot_index(listData = [A_gg_cor_avg, B_gg_cor_avg], saveAs='GeneIdxCor_Jonas_01.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$R(g_i,g_j)$",  listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0.8,0.9, 1],               legend_loc='lower right', lsize =20, EB = [EB_A_gg_cor, EB_B_gg_cor])
    A_obj.Plot_index(listData = [A_cc_cor_avg, B_cc_cor_avg], saveAs='cellIdxCor_Jonas_01.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$R(C_k,C_l)$",  listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.4,0.8],          legend_loc='lower right', lsize =20, EB = [EB_A_cc_cor, EB_B_cc_cor])
    A_obj.Plot_index(listData = [A_idx_cor, B_idx_cor],       saveAs='IdxCor_Jonas_01.pdf',     listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$I_c$",         listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [1, 1.5, 2], legend_loc='lower right', lsize =20, EB = [EB_A_idx_cor, EB_B_idx_cor])
    
    A_obj.Plot_index(listData = [A_gg_MI_avg_1, B_gg_MI_avg_1], saveAs='GeneIdxMI_Jonas_1.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI(g_i,g_j)$", listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.010, 0.02],         legend_loc='lower right', lsize =18, EB = [EB_A_gg_MI_1, EB_B_gg_MI_1])
    A_obj.Plot_index(listData = [A_cc_MI_avg_1, B_cc_MI_avg_1], saveAs='cellIdxMI_Jonas_1.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI(C_k,C_l)$", listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.1, 0.2],            legend_loc='lower right', lsize =18, EB = [EB_A_cc_MI_1, EB_B_cc_MI_1])
    A_obj.Plot_index(listData = [A_idx_MI_1 , B_idx_MI_1 ],     saveAs='IdxMI_Jonas_1.pdf',     listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI_c$",        listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0,0.1, 0.2],               legend_loc='lower right', lsize =18, EB = [EB_A_idx_MI_1, EB_B_idx_MI_1])
   
    A_obj.Plot_index(listData = [A_gg_MI_avg_2, B_gg_MI_avg_2], saveAs='GeneIdxMI_Jonas_2.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI(g_i,g_j)$", listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.01, 0.02],  legend_loc='lower right', lsize =18, EB = [EB_A_gg_MI_2, EB_B_gg_MI_2])
    A_obj.Plot_index(listData = [A_cc_MI_avg_2, B_cc_MI_avg_2], saveAs='cellIdxMI_Jonas_2.pdf', listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI(C_k,C_l)$", listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.06, 0.12],  legend_loc='lower right', lsize =18, EB = [EB_A_cc_MI_2, EB_B_cc_MI_2])
    A_obj.Plot_index(listData = [A_idx_MI_2 , B_idx_MI_2],      saveAs='IdxMI_Jonas_2.pdf',     listColor = ['red','grey'], listLabels = ['Mutant','Control'], xLabel = "Time (days)", yLabel = r"$MI_c$",       listOfSteps=[0,8,14,42], currentNamesList = ['d0','d10','d14','d42'], ytik = [0, 0.15, 0.3], legend_loc='lower right', lsize =18, EB = [EB_A_idx_MI_2, EB_B_idx_MI_2])
    
#%%

blood_cell = 1
if blood_cell == 1:
    """
    Same analysis for the sc-qPCR blood cell data.
    """
    GetDataFromFiles_BC = 1
    GetCellMatrixDat_BC = 1
    get_obj = 1
    correlation_BC = 1
    mutual_info_BC = 1
    plotFig_BC = 1
    Gibbs_energy_BC = 1
    
    if GetDataFromFiles_BC == 1: 
       """
       Load  data and deleting 2 house keeping genes.
       """
       geneNames_BC, cellNames_BC, expressionValues_BC = GF.My_get_from_txt('data/bloodCell_data/allcells_sorted.txt')
       expressionValues_BC = np.delete(expressionValues_BC, (4,18), axis=1)#TBP and GAPDH genes have 4 and 18 indices, therefore I delete them
       expressionValues_Array_BC = np.array(expressionValues_BC)
       expressionValues_Array_Transposed_BC = np.transpose(expressionValues_Array_BC)
       geneNames_BC.remove(geneNames_BC[4])
       geneNames_BC.remove(geneNames_BC[17])
       geneNames_BC.remove(geneNames_BC[16])
   

#%% -------------------------- Seprate cells by cell types ---------------------
GetCellMatrixDat_BC = 1
if GetCellMatrixDat_BC == 1:
       """
       Separation of the data for each cell-type and each time point.
       """
       
       
       EML_Matrix = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'EML')
       ERY_Matrix = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'ERY')
       MYL_Matrix = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'MYL')
       COM_Matrix = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'COM')
       ##___________________sperate ERY by its three time points----------------------
       ERY_Matrix_D1 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'ERY-D1')  
       ERY_Matrix_D3 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'ERY-D3')  
       ERY_Matrix_D6 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'ERY-D6')  

       ##___________________sperate MYL by its three time points----------------------

       MYL_Matrix_D1 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'MYL-D1')  
       MYL_Matrix_D3 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'MYL-D3')  
       MYL_Matrix_D6 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'MYL-D6')
       ##___________________sperate COM by its three time points----------------------

       COM_Matrix_D1 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'COM-D1')  
       COM_Matrix_D3 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'COM-D3')  
       COM_Matrix_D6 = GF.get_cell_matrix(cellNames_BC, expressionValues_BC, 'COM-D6')   
      
       ERY_data = [EML_Matrix, ERY_Matrix_D1, ERY_Matrix_D3, ERY_Matrix_D6]
       MYL_data = [EML_Matrix, MYL_Matrix_D1, MYL_Matrix_D3, MYL_Matrix_D6]
       COM_data = [EML_Matrix, COM_Matrix_D1, COM_Matrix_D3, COM_Matrix_D6]
       
       ERY_genes = [geneNames_BC, geneNames_BC, geneNames_BC, geneNames_BC]
       MYL_genes = [geneNames_BC, geneNames_BC, geneNames_BC, geneNames_BC]
       COM_genes = [geneNames_BC, geneNames_BC, geneNames_BC, geneNames_BC]

       """
       Initialization of one object for each cell-type.
       """       
       if get_obj == 1:  

          ERY_obj = CriticalIndex(ERY_data, ERY_genes)
          ERY_b1, ERY_b2 = ERY_obj.binarization()
          ERY_com_gen = ERY_obj.com_genes(ERY_genes)

          MYL_obj = CriticalIndex(MYL_data, MYL_genes)
          MYL_b1, MYL_b2 = MYL_obj.binarization()
          MYL_com_gen = MYL_obj.com_genes(MYL_genes)
       
          COM_obj = CriticalIndex(COM_data, COM_genes)
          COM_b1, COM_b2 = COM_obj.binarization()
          COM_com_gen = COM_obj.com_genes(COM_genes)

          ERY_com_gen = ERY_obj.com_genes(ERY_genes)
          ERY_b1, ERY_b2 = ERY_obj.binarization()

          MYL_obj = CriticalIndex(MYL_data, MYL_genes)
          MYL_com_gen = MYL_obj.com_genes(MYL_genes)
          MYL_b1, MYL_b2 = MYL_obj.binarization()

          COM_obj = CriticalIndex(COM_data, COM_genes)
          COM_com_gen = COM_obj.com_genes(COM_genes)
          COM_b1, COM_b2 = COM_obj.binarization()
#%%
       """
       Computation of correlation matrices for each cell-type and subsequent indices and error bars.
       """    
       if correlation_BC == 1:

          ERY_gg_cor,  ERY_cc_cor = ERY_obj.Correlation(1)
          ERY_gg_cor_avg, ERY_cc_cor_avg, ERY_idx_cor = ERY_obj.Index(ERY_gg_cor, ERY_cc_cor)
          EB_ERY_gg_cor, EB_ERY_cc_cor, EB_ERY_idx_cor = ERY_obj.EB_SEM(ERY_gg_cor_avg, ERY_cc_cor_avg, ERY_gg_cor, ERY_cc_cor)
        
          MYL_gg_cor,  MYL_cc_cor = MYL_obj.Correlation(1)
          MYL_gg_cor_avg, MYL_cc_cor_avg, MYL_idx_cor = MYL_obj.Index(MYL_gg_cor, MYL_cc_cor)
          EB_MYL_gg_cor, EB_MYL_cc_cor, EB_MYL_idx_cor = MYL_obj.EB_SEM(MYL_gg_cor_avg, MYL_cc_cor_avg, MYL_gg_cor, MYL_cc_cor) 
        
          COM_gg_cor,  COM_cc_cor = COM_obj.Correlation(1)
          COM_gg_cor_avg, COM_cc_cor_avg, COM_idx_cor = COM_obj.Index(COM_gg_cor, COM_cc_cor)
          EB_COM_gg_cor, EB_COM_cc_cor, EB_COM_idx_cor = COM_obj.EB_SEM(COM_gg_cor_avg, COM_cc_cor_avg, COM_gg_cor, COM_cc_cor)    


#%% 
       """
       Computation of MI matrices for each cell-type and subsequent indices and error bars for the two binarization methods.
       """            
       if mutual_info_BC == 1:

          ERY_gg_MI_1, ERY_cc_MI_1 = ERY_obj.Mutual_information(0, 'on_off')
          ERY_gg_MI_avg_1, ERY_cc_MI_avg_1, ERY_MI_idx_1 = ERY_obj.Index(ERY_gg_MI_1, ERY_cc_MI_1)
          EB_ERY_gg_MI_1, EB_ERY_cc_MI_1, EB_ERY_idx_MI_1 = ERY_obj.EB_SEM(ERY_gg_MI_avg_1, ERY_cc_MI_avg_1, ERY_gg_MI_1, ERY_cc_MI_1) 
       
          MYL_gg_MI_1, MYL_cc_MI_1 = MYL_obj.Mutual_information(0, 'on_off')
          MYL_gg_MI_avg_1, MYL_cc_MI_avg_1, MYL_MI_idx_1 = MYL_obj.Index(MYL_gg_MI_1, MYL_cc_MI_1)
          EB_MYL_gg_MI_1, EB_MYL_cc_MI_1, EB_MYL_idx_MI_1 = MYL_obj.EB_SEM(MYL_gg_MI_avg_1, MYL_cc_MI_avg_1, MYL_gg_MI_1, MYL_cc_MI_1) 
       
          COM_gg_MI_1, COM_cc_MI_1 = COM_obj.Mutual_information(0, 'on_off')
          COM_gg_MI_avg_1, COM_cc_MI_avg_1, COM_MI_idx_1 = COM_obj.Index(COM_gg_MI_1, COM_cc_MI_1)
          EB_COM_gg_MI_1, EB_COM_cc_MI_1, EB_COM_idx_MI_1 = COM_obj.EB_SEM(COM_gg_MI_avg_1, COM_cc_MI_avg_1, COM_gg_MI_1, COM_cc_MI_1) 
       

          ERY_gg_MI_2, ERY_cc_MI_2 = ERY_obj.Mutual_information(0, 'median_all')
          ERY_gg_MI_avg_2, ERY_cc_MI_avg_2, ERY_MI_idx_2 = ERY_obj.Index(ERY_gg_MI_2, ERY_cc_MI_2)
          EB_ERY_gg_MI_2, EB_ERY_cc_MI_2, EB_ERY_idx_MI_2 = ERY_obj.EB_SEM(ERY_gg_MI_avg_2, ERY_cc_MI_avg_2, ERY_gg_MI_2, ERY_cc_MI_2) 
       
          MYL_gg_MI_2, MYL_cc_MI_2 = MYL_obj.Mutual_information(0, 'median_all')
          MYL_gg_MI_avg_2, MYL_cc_MI_avg_2, MYL_MI_idx_2 = MYL_obj.Index(MYL_gg_MI_2, MYL_cc_MI_2)
          EB_MYL_gg_MI_2, EB_MYL_cc_MI_2, EB_MYL_idx_MI_2 = MYL_obj.EB_SEM(MYL_gg_MI_avg_2, MYL_cc_MI_avg_2, MYL_gg_MI_2, MYL_cc_MI_2) 
       
          COM_gg_MI_2, COM_cc_MI_2 = COM_obj.Mutual_information(0, 'median_all')
          COM_gg_MI_avg_2, COM_cc_MI_avg_2, COM_MI_idx_2 = COM_obj.Index(COM_gg_MI_2, COM_cc_MI_2)
          EB_COM_gg_MI_2, EB_COM_cc_MI_2, EB_COM_idx_MI_2 = COM_obj.EB_SEM(COM_gg_MI_avg_2, COM_cc_MI_avg_2, COM_gg_MI_2, COM_cc_MI_2)   
           

#%%
       """
       Plotting indices for correlation and MI_1 and MI_2.
       """       
       if plotFig_BC == 1:

    
          ERY_obj.Plot_index(listData = [ERY_gg_cor_avg, MYL_gg_cor_avg, COM_gg_cor_avg],  saveAs='GeneIdxCor_BC_001.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'],  xLabel = "Time (days)", yLabel = r"$R(g_i,g_j)$", listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'], ytik =[0,0.1,0.2],legend_loc='lower center',lsize =18, EB = [EB_ERY_gg_cor,  EB_MYL_gg_cor,  EB_COM_gg_cor])
          ERY_obj.Plot_index(listData = [ERY_cc_cor_avg, MYL_cc_cor_avg, COM_cc_cor_avg],  saveAs='cellIdxCor_BC_001.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'],  xLabel = "Time (days)", yLabel = r"$R(C_k,C_l)$", listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'], ytik =[0,0.4,0.8],legend_loc='lower right',lsize =18, EB = [EB_ERY_cc_cor,  EB_MYL_cc_cor,  EB_COM_cc_cor])
          ERY_obj.Plot_index(listData = [ERY_idx_cor,    MYL_idx_cor,    COM_idx_cor],     saveAs='IdxCor_BC_001.pdf',     listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'],  xLabel = "Time (days)", yLabel = r"$I_c$",        listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'], ytik =[0,0.2,0.4], legend_loc='lower center',lsize =18, EB = [EB_ERY_idx_cor, EB_MYL_idx_cor, EB_COM_idx_cor])
    
          ERY_obj.Plot_index(listData = [ERY_gg_MI_avg_1, MYL_gg_MI_avg_1, COM_gg_MI_avg_1], saveAs='GeneIdxMI_BC_1.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI(g_i,g_j)$",  ytik =[0,0.04,0.08], listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='lower right',lsize =18, EB = [EB_ERY_gg_MI_1,  EB_MYL_gg_MI_1,  EB_COM_gg_MI_1])
          ERY_obj.Plot_index(listData = [ERY_cc_MI_avg_1, MYL_cc_MI_avg_1, COM_cc_MI_avg_1], saveAs='cellIdxMI_BC_1.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI(C_k,C_l)$",  ytik =[0,0.2,0.4],   listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='lower right', lsize =18,EB = [EB_ERY_cc_MI_1,  EB_MYL_cc_MI_1,  EB_COM_cc_MI_1])
          ERY_obj.Plot_index(listData = [ERY_MI_idx_1,    MYL_MI_idx_1,    COM_MI_idx_1],    saveAs='IdxMI_BC_1.pdf',     listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI_c$",         ytik =[0,0.2,0.4],   listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='lower center', lsize =18,EB = [EB_ERY_idx_MI_1, EB_MYL_idx_MI_1, EB_COM_idx_MI_1])
   
          ERY_obj.Plot_index(listData = [ERY_gg_MI_avg_2, MYL_gg_MI_avg_2, COM_gg_MI_avg_2], saveAs='GeneIdxMI_BC_2.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI(g_i,g_j)$", ytik =[0,0.04,0.08, 0.12], listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='lower right',lsize =18, EB = [EB_ERY_gg_MI_2, EB_MYL_gg_MI_2, EB_COM_gg_MI_2])
          ERY_obj.Plot_index(listData = [ERY_cc_MI_avg_2, MYL_cc_MI_avg_2, COM_cc_MI_avg_2], saveAs='cellIdxMI_BC_2.pdf', listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI(C_k,C_l)$", ytik =[0,0.2,0.4], listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='upper right',lsize =18, EB = [EB_ERY_cc_MI_2, EB_MYL_cc_MI_2, EB_COM_cc_MI_2])
          ERY_obj.Plot_index(listData = [ERY_MI_idx_2,    MYL_MI_idx_2,    COM_MI_idx_2],    saveAs='IdxMI_BC_2.pdf',     listColor = ['dodgerblue', 'orange', 'limegreen'], listLabels = ['EPO', 'GM-CSF/IL-3', 'Combination'], xLabel = "Time (days)", yLabel = r"$MI_c$",     ytik =[0,0.5,1, 1.5],   listOfSteps=[0,1,3,6], currentNamesList = ['d0','d1','d3','d6'],legend_loc='lower center',lsize =16, EB = [EB_ERY_idx_MI_2, EB_MYL_idx_MI_2, EB_COM_idx_MI_2])
       
       
