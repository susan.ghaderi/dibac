#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script computes differentially expresses genes (DEG) between two 
conditions (like the mutant and control treatments in the manuscript) by Scanpy and the MI approach.
The Venn diagram of these treatment with two different p-values and comapring
 the DEGs are plotted in Figures 2 and 4.

Created on Tue Aug 10 17:16:49 2021

@author: susanghaderi
"""

import numpy as np
import pandas as pd

from Criticl_index_computation import CriticalIndex

import anndata as ad
import scanpy as sc
import os


#%%
A_cut_cell = np.load(os.path.join('data/IPS_data', 'A_cut_cell.npy'),allow_pickle=True)
B_cut_cell = np.load(os.path.join('data/IPS_data', 'B_cut_cell.npy'),allow_pickle=True)

A_geneNames = np.load(os.path.join('data/IPS_data','A_geneNames.npy'),allow_pickle=True)
B_geneNames = np.load(os.path.join('data/IPS_data','B_geneNames.npy'),allow_pickle=True)

A_obj = CriticalIndex(A_cut_cell, A_geneNames) 
""" Determining the common gene Names between A and B """
common_genes = A_obj.com_genes(A_geneNames, B_geneNames)
"""final data containing common genes"""
A = A_obj.final_data()


B_obj = CriticalIndex(B_cut_cell, B_geneNames)
common_genes = B_obj.com_genes(A_geneNames, B_geneNames)
B = B_obj.final_data()

g = common_genes

#%% 
"""
Computation of DEGs for iPSC data
"""
C0 = np.concatenate((A[0],B[0]), axis=1).T
C10 = np.concatenate((A[1],B[1]), axis=1).T
C14 = np.concatenate((A[2],B[2]), axis=1).T
C42 = np.concatenate((A[3],B[3]), axis=1).T

n_obs1 = 250
obs = pd.DataFrame()
obs['cellNames'] = ['Mutant day 0' for i in range(1, n_obs1+1)]+['Control day 0' for i in range(1, n_obs1+1)]
var_names =  g#[i*'C' for i in range(1, n_obs+1)]
var = pd.DataFrame(index=var_names)
adata = ad.AnnData(C0, obs=obs, var=var, dtype='int32')
adata.obs.groupby(['cellNames']).groups

obs2 = pd.DataFrame()
obs2['cellNames'] = ['Mutant day 10' for i in range(1, n_obs1+1)]+['Control day 10' for i in range(1, n_obs1+1)]
adata_10 = ad.AnnData(C10, obs=obs2, var=var, dtype='int32')

obs3 = pd.DataFrame()
obs3['cellNames'] = ['Mutant day 14' for i in range(1, n_obs1+1)]+['Control day 14' for i in range(1, n_obs1+1)]
adata_14 = ad.AnnData(C14, obs=obs3, var=var, dtype='int32')

obs4 = pd.DataFrame()
obs4['cellNames'] = ['Mutant day 42' for i in range(1, n_obs1+1)]+['Control day 42' for i in range(1, n_obs1+1)]
adata_42 = ad.AnnData(C42, obs=obs4, var=var, dtype='int32')
    
#%%
sc.tl.rank_genes_groups(adata, 'cellNames', groups=['Mutant day 0'], reference='Control day 0', method='wilcoxon')
#sc.tl.rank_genes_groups(adata, 'cellNames', method='wilcoxon')

sc.tl.rank_genes_groups(adata_10, 'cellNames', groups=['Mutant day 10'], reference='Control day 10', method='wilcoxon')
#sc.tl.rank_genes_groups(adata_10, 'cellNames', method='wilcoxon')

sc.tl.rank_genes_groups(adata_14, 'cellNames', groups=['Mutant day 14'], reference='Control day 14', method='wilcoxon')
#sc.tl.rank_genes_groups(adata_14, 'cellNames', method='wilcoxon')

sc.tl.rank_genes_groups(adata_42, 'cellNames', groups=['Mutant day 42'], reference='Control day 42', method='wilcoxon')
#sc.tl.rank_genes_groups(adata_42, 'cellNames', method='wilcoxon')       
#%%
""" Plotting of heat maps of gene-gene and cell-cell correlations """

#%%
def return_deglist(adata, n_genes =16662 , ax=0):
    """
    Function returning list of gene names and their adjusted p-values

    Parameters
    ----------
    adata : TYPE
        DESCRIPTION.
    n_genes : TYPE, optional
        DESCRIPTION. The default is 16662.
    ax : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    d : TYPE
        DESCRIPTION.
    pval : TYPE
        DESCRIPTION.

    """
    l = adata.uns['rank_genes_groups']['names']
    p = list(adata.uns['rank_genes_groups']['pvals_adj'])

    d = []
    pval = []
    for i in range(len(p)):
        d.append(l[i][ax])
        pval.append(p[i][ax])
        
    return d , pval
#%%   
g0_m,  pv0_m = return_deglist(adata)
g10_m, pv10_m = return_deglist(adata_10)
g14_m, pv14_m = return_deglist(adata_14)
g42_m, pv42_m = return_deglist(adata_42)

#%% 

def DEG_list(g, p):
    """
    This function filters the genes by their adjusted p-values

    Parameters
    ----------
    g : TYPE
        DESCRIPTION.
    p : TYPE
        DESCRIPTION.

    Returns
    -------
    DEG : TYPE
        DESCRIPTION.
    DEG_pval : TYPE
        DESCRIPTION.

    """
    p_val = 0.05
   # for i in range(g):
    d = []
    for i in range(len(p)):
        if p[i]< (p_val/(4*len(p))):
            d.append(i)
    DEG = []
    DEG_pval =[]        
    for i in d:       
        DEG.append(g[i]) 
        DEG_pval.append(p[i])
        
    return DEG, DEG_pval
      
#%% 
DEG_0,  DEG_pval_0  = DEG_list(g0_m, pv0_m)         
DEG_10, DEG_pval_10 = DEG_list(g10_m, pv10_m)         
DEG_14, DEG_pval_14 = DEG_list(g14_m, pv14_m)         
DEG_42, DEG_pval_42 = DEG_list(g42_m, pv42_m)   

with open("COM_genes.txt", 'w') as output:
    for row in g:
        output.write(str(row) + '\n')

with open("DEG0_a.txt", 'w') as output:
    for row in DEG_0:
        output.write(str(row) + '\n')

with open("DEG10_a.txt", 'w') as output:
    for row in DEG_10:
        output.write(str(row) + '\n')
        
with open("DEG14_a.txt", 'w') as output:
    for row in DEG_14:
        output.write(str(row) + '\n')
        
with open("DEG42_a.txt", 'w') as output:
    for row in DEG_42:
        output.write(str(row) + '\n')
        
        
DEG_0a  = pd.DataFrame(data ={'gene names': DEG_0, 'pvals_adj': DEG_pval_0})
DEG_10a  = pd.DataFrame(data ={'gene names': DEG_10, 'pvals_adj': DEG_pval_10})
DEG_14a  = pd.DataFrame(data ={'gene names': DEG_14, 'pvals_adj': DEG_pval_14})
DEG_42a  = pd.DataFrame(data ={'gene names': DEG_42, 'pvals_adj': DEG_pval_42})

DEG_0a.to_csv(os.path.join('data/DEG', 'DEG_0_scanpy.csv'))
DEG_10a.to_csv(os.path.join('data/DEG','DEG_10_scanpy.csv'))
DEG_14a.to_csv(os.path.join('data/DEG', 'DEG_14_scanpy.csv'))
DEG_42a.to_csv(os.path.join('data/DEG','DEG_42_scanpy.csv'))

      
#%%
A_KL = np.load(os.path.join('data/KL','KL_new_A.npy'))
B_KL = np.load(os.path.join('data/KL','KL_new_B.npy'))
KL_g = np.load(os.path.join('data/KL','new_AB_gene.npy'))
#%% Here we save the 10 percent high KL values and their corresponding genes
high_KL_10per_GN_a0  = KL_g[(-A_KL[0]).argsort()[0:400]]
high_KL_10per_GN_a10 = KL_g[(-A_KL[1]).argsort()[0:400]]
high_KL_10per_GN_a14 = KL_g[(-A_KL[2]).argsort()[0:400]]
high_KL_10per_GN_a42 = KL_g[(-A_KL[3]).argsort()[0:400]]
  
high_KL_10per_GN_b0  = KL_g[(-B_KL[0]).argsort()[0:400]]
high_KL_10per_GN_b10 = KL_g[(-B_KL[1]).argsort()[0:400]]
high_KL_10per_GN_b14 = KL_g[(-B_KL[2]).argsort()[0:400]]
high_KL_10per_GN_b42 = KL_g[(-B_KL[3]).argsort()[0:400]]

high_KL_10per_a0  = A_KL[0][(-A_KL[0]).argsort()[0:400]]
high_KL_10per_a10 = A_KL[1][(-A_KL[1]).argsort()[0:400]]
high_KL_10per_a14 = A_KL[2][(-A_KL[2]).argsort()[0:400]]
high_KL_10per_a42 = A_KL[3][(-A_KL[3]).argsort()[0:400]]
  
high_KL_10per_b0  = B_KL[0][(-B_KL[0]).argsort()[0:400]]
high_KL_10per_b10 = B_KL[1][(-B_KL[1]).argsort()[0:400]]
high_KL_10per_b14 = B_KL[2][(-B_KL[2]).argsort()[0:400]]
high_KL_10per_b42 = B_KL[3][(-B_KL[3]).argsort()[0:400]]

high_KL_a_0   = pd.DataFrame(data ={'gene names': high_KL_10per_GN_a0,  'pvals_adj': high_KL_10per_a0})
high_KL_a_10  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_a10, 'pvals_adj': high_KL_10per_a10})
high_KL_a_14  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_a14, 'pvals_adj': high_KL_10per_a14})
high_KL_a_42  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_a42, 'pvals_adj': high_KL_10per_a42})


high_KL_b_0   = pd.DataFrame(data ={'gene names': high_KL_10per_GN_b0,  'pvals_adj': high_KL_10per_b0})
high_KL_b_10  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_b10, 'pvals_adj': high_KL_10per_b10})
high_KL_b_14  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_b14, 'pvals_adj': high_KL_10per_b14})
high_KL_b_42  = pd.DataFrame(data ={'gene names': high_KL_10per_GN_b42, 'pvals_adj': high_KL_10per_b42})

high_KL_a_0.to_csv(os.path.join('data/DEG/high_DEG' , 'high_KL_a_0.csv'))
high_KL_a_10.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_a_10.csv'))
high_KL_a_14.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_a_14.csv'))
high_KL_a_42.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_a_42.csv'))

high_KL_b_0.to_csv(os.path.join('data/DEG/high_DEG' , 'high_KL_b_0.csv'))
high_KL_b_10.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_b_10.csv'))
high_KL_b_14.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_b_14.csv'))
high_KL_b_42.to_csv(os.path.join('data/DEG/high_DEG', 'high_KL_b_42.csv'))

#%%
# Using set() 
def Diff2(list1, list2): 
    return list(set(list1)-set(list2))

diffhigh_KLfromDEG_0_mutant =  Diff2(high_KL_10per_a0, DEG_0) 
diffhigh_KLfromDEG_10_mutant =  Diff2(high_KL_10per_a10, DEG_10)   
diffhigh_KLfromDEG_14_mutant =  Diff2(high_KL_10per_a14, DEG_14)   
diffhigh_KLfromDEG_42_mutant =  Diff2(high_KL_10per_a42, DEG_42)   

    
Diffhigh_KLfromDEG_0_mutant  = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_0_mutant})
Diffhigh_KLfromDEG_10_mutant = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_10_mutant})
Diffhigh_KLfromDEG_14_mutant  = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_14_mutant})
Diffhigh_KLfromDEG_42_mutant  = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_42_mutant})

Diffhigh_KLfromDEG_0_mutant.to_csv(os.path.join('data/DEG/high_KL_scan' , 'Diff_high_KLsubDEG_0_mutant.txt'))
Diffhigh_KLfromDEG_10_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_10_mutant.txt'))
Diffhigh_KLfromDEG_14_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_14_mutant.txt'))
Diffhigh_KLfromDEG_42_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_42_mutant.txt'))

diffhigh_KLfromDEG_0_control=  Diff2(high_KL_10per_b0, DEG_0) 
diffhigh_KLfromDEG_10_control=  Diff2(high_KL_10per_b10, DEG_10)   
diffhigh_KLfromDEG_14_control=  Diff2(high_KL_10per_b14, DEG_14)   
diffhigh_KLfromDEG_42_control=  Diff2(high_KL_10per_b42, DEG_42)   

Diffhigh_KLfromDEG_0_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_0_control})
Diffhigh_KLfromDEG_10_control= pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_10_control})
Diffhigh_KLfromDEG_14_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_14_control})
Diffhigh_KLfromDEG_42_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_42_control})


Diffhigh_KLfromDEG_0_control.to_csv(os.path.join('data/DEG/high_KL_scan',  'Diff_high_KLsubDEG_0_control.txt'))
Diffhigh_KLfromDEG_10_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_10_control.txt'))
Diffhigh_KLfromDEG_14_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_14_control.txt'))
Diffhigh_KLfromDEG_42_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsubDEG_42_control.txt'))
#%%
# Using set() 
def Diff3(list1, list2): 
    return list(set(list2)-set(list1))

diffhigh_DEGfromKL_0_mutant =  Diff3(high_KL_10per_a0, DEG_0) 
diffhigh_DEGfromKL_10_mutant =  Diff3(high_KL_10per_a10, DEG_10)   
diffhigh_DEGfromKL_14_mutant =  Diff3(high_KL_10per_a14, DEG_14)   
diffhigh_DEGfromKL_42_mutant =  Diff3(high_KL_10per_a42, DEG_42)   

    
Diffhigh_DEGfromKL_0_mutant  = pd.DataFrame(data ={'gene names': diffhigh_DEGfromKL_0_mutant})
Diffhigh_DEGfromKL_10_mutant = pd.DataFrame(data ={'gene names': diffhigh_DEGfromKL_10_mutant})
Diffhigh_DEGfromKL_14_mutant  = pd.DataFrame(data ={'gene names': diffhigh_DEGfromKL_14_mutant})
Diffhigh_DEGfromKL_42_mutant  = pd.DataFrame(data ={'gene names': diffhigh_DEGfromKL_42_mutant})

Diffhigh_DEGfromKL_0_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_DEGsub_high_KL_0_mutant.txt'))
Diffhigh_DEGfromKL_10_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_DEGsub_high_KL_10_mutant.txt'))
Diffhigh_DEGfromKL_14_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_DEGsub_high_KL_14_mutant.txt'))
Diffhigh_DEGfromKL_42_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_DEGsub_high_KL_42_mutant.txt'))

diffhigh_DEGfromKL_0_control=  Diff3(high_KL_10per_b0, DEG_0) 
diffhigh_DEGfromKL_10_control=  Diff3(high_KL_10per_b10, DEG_10)   
diffhigh_DEGfromKL_14_control=  Diff3(high_KL_10per_b14, DEG_14)   
diffhigh_DEGfromKL_42_control=  Diff3(high_KL_10per_b42, DEG_42)   

Diffhigh_KLfromDEG_0_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_0_control})
Diffhigh_KLfromDEG_10_control= pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_10_control})
Diffhigh_KLfromDEG_14_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_14_control})
Diffhigh_KLfromDEG_42_control = pd.DataFrame(data ={'gene names': diffhigh_KLfromDEG_42_control})

Diffhigh_KLfromDEG_0_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsub_high_DEG_0_control.txt'))
Diffhigh_KLfromDEG_10_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsub_high_DEG_10_control.txt'))
Diffhigh_KLfromDEG_14_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsub_high_DEG_14_control.txt'))
Diffhigh_KLfromDEG_42_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'Diff_high_KLsub_high_DEG_42_control.txt'))



#%%  high KL and DEG
high_KLandDEG_0_A = list(set(high_KL_10per_a0)&set(DEG_0)) 
high_KLandDEG_10_A = list(set(high_KL_10per_a10)&set(DEG_10))
high_KLandDEG_14_A = list(set(high_KL_10per_a14)&set(DEG_14))
high_KLandDEG_42_A = list(set(high_KL_10per_a42)&set(DEG_42))

high_KLandDEG_0_B = list(set(high_KL_10per_b0)&set(DEG_0))
high_KLandDEG_10_B = list(set(high_KL_10per_b10)&set(DEG_10))
high_KLandDEG_14_B = list(set(high_KL_10per_b14)&set(DEG_14))
high_KLandDEG_42_B = list(set(high_KL_10per_b42)&set(DEG_42))


high_KLandDEG_0_mutant  = pd.DataFrame(data ={'gene names': high_KLandDEG_0_A})
high_KLandDEG_10_mutant = pd.DataFrame(data ={'gene names': high_KLandDEG_10_A})
high_KLandDEG_14_mutant  = pd.DataFrame(data ={'gene names': high_KLandDEG_14_A})
high_KLandDEG_42_mutant  = pd.DataFrame(data ={'gene names': high_KLandDEG_42_A})

high_KLandDEG_0_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_0_mutant.txt'))
high_KLandDEG_10_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_10_mutant.txt'))
high_KLandDEG_14_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_14_mutant.txt'))
high_KLandDEG_42_mutant.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_42_mutant.txt'))

high_KLandDEG_0_control = pd.DataFrame(data ={'gene names': high_KLandDEG_0_B})
high_KLandDEG_10_control= pd.DataFrame(data ={'gene names': high_KLandDEG_10_B})
high_KLandDEG_14_control = pd.DataFrame(data ={'gene names': high_KLandDEG_14_B})
high_KLandDEG_42_control = pd.DataFrame(data ={'gene names': high_KLandDEG_42_B})

high_KLandDEG_0_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_0_control.txt'))
high_KLandDEG_10_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_10_control.txt'))
high_KLandDEG_14_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_14_control.txt'))
high_KLandDEG_42_control.to_csv(os.path.join('data/DEG/high_KL_scan', 'high_KL_and_DEG_42_control.txt'))


#%%
"""Before running this section, the SOM function has to be called to have access to the high_SOM files"""
som_0a = pd.read_csv('data/SOM/', 'A_D_0_n.txt', delimiter=',')['gene names']
som_10a = pd.read_csv('data/SOM/', 'A_D_10_n.txt', delimiter=',')['gene names']
som_14a = pd.read_csv('data/SOM/', 'A_D_14_n.txt', delimiter=',')['gene names']
som_42a = pd.read_csv('data/SOM/', 'A_D_42_n.txt', delimiter=',')['gene names']

som_0b = pd.read_csv('data/SOM/', 'B_D_0_n.txt', delimiter=',')['gene names']
som_10b = pd.read_csv('data/SOM/', 'B_D_10_n.txt', delimiter=',')['gene names']
som_14b = pd.read_csv('data/SOM/', 'B_D_14_n.txt', delimiter=',')['gene names']
som_42b = pd.read_csv('data/SOM/', 'B_D_42_n.txt', delimiter=',')['gene names']



#%%
s0_m = set(som_0a)&set(DEG_0) 
s10_m = set(som_10a)&set(DEG_10) 
s14_m = set(som_14a)&set(DEG_14) 
s42_m = set(som_42a)&set(DEG_42) 

s0_c = set(som_0b)&set(DEG_0) 
s10_c = set(som_10b)&set(DEG_10) 
s14_c = set(som_14b)&set(DEG_14) 
s42_c = set(som_42b)&set(DEG_42) 

#%%
High_SOM_A0    = pd.read_csv('data/SOM/', 'A_0_high_SOM.txt', delimiter=',')['gene names']
High_SOM_A10   = pd.read_csv('data/SOM/', 'A_SOM_10_n.txt'  , delimiter=',')['gene names']
High_SOM_A14   = pd.read_csv('data/SOM/', 'A_SOM_14_n.txt' , delimiter=',')['gene names']

High_SOM_A42_1 =  pd.read_csv('data/SOM/', 'A_42_1_high_SOM.txt', delimiter=',')['gene names']
High_SOM_A42_2 =  pd.read_csv('data/SOM/', 'A_42_2_high_SOM.txt', delimiter=',')['gene names']
High_SOM_A42_3 =  pd.read_csv('data/SOM/', 'A_42_3_high_SOM.txt', delimiter=',')['gene names']

High_SOM_B0    = pd.read_csv('data/SOM/', 'B_0_high_SOM.txt', delimiter=',')['gene names']
High_SOM_B10   = pd.read_csv('data/SOM/', 'B_SOM_10_n.txt'  , delimiter=',')['gene names']
High_SOM_B14   = pd.read_csv('data/SOM/', 'B_SOM_14_n.txt'  , delimiter=',')['gene names']

High_SOM_B42_1 =  pd.read_csv('data/SOM/', 'B_42_1_high_SOM.txt', delimiter=',')['gene names']
High_SOM_B42_2 =  pd.read_csv('data/SOM/', 'B_42_2_high_SOM.txt', delimiter=',')['gene names']
High_SOM_B42_3 =  pd.read_csv('data/SOM/', 'B_42_3_high_SOM.txt', delimiter=',')['gene names']
High_SOM_B42_4 =  pd.read_csv('data/SOM/', 'B_42_4_high_SOM.txt', delimiter=',')['gene names']


