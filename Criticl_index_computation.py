#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script is one of the main scripts that contains the set of functions'
to compute correlation and MI indices together with theheatmaps plotted in Fig. 3

Created on Mon Jul  1 14:32:36 2019

@author: susan.ghaderi
"""

import numpy as np
import General_Functions as GF
import matplotlib.pyplot as plt
import math
import Script_data as sd
from functools import reduce
import os
class CriticalIndex:
    
    def __init__(self, data, geneNames):
        self.data = data
        self.ld = int(len(data))
        self.idx_BS = None
        self.geneNames = geneNames
        self.common_genes = None
        self.data_binary_1 = None
        self.data_binary_2 = None
        self.data_binary_3 = None
        self.geneNames_BS = None

        
    def cutCells(self,n):
        """this function cuts n cells from the DEM
        input: n: number of cells that should be cutted
        output: new data with nu number of cells    
        """
        data = self.data
        ld = self.ld
        data_cut = []
        for i in range(ld):
            data_cut.append(sd.get_top_cells_matrix(data[i],n)[0])
        self.data = data_cut
        return self.data
    
    def com_genes(self, geneNames_1, geneNames_2 = None):
        """
        This function retruns the common genes between two list of 
        gene Names
        input: 
            geneNames_1:  ld list of gene names lists
            geneNames_2:  ld list of gene names lists
        output:
            common_genes: a list of common genes between geneNames_1 and 
            geneNames_1 
        """
        commom_1 = reduce(np.intersect1d, (geneNames_1))
        if geneNames_2 is not None:
            commom_2 = reduce(np.intersect1d, (geneNames_2))
            intersec_genes = reduce(np.intersect1d, (commom_1, commom_2))
        else:
            intersec_genes = commom_1
        self.common_genes = list(intersec_genes)
        return self.common_genes
    
    def final_data(self):
        """
        This function identifies common genes and produces data sets with the same
        common genes.
        """
        data = self.data[:]
        ld = self.ld
        geneNames = self.geneNames
        common_genes = self.common_genes[:]
        data_2 = []
        for i in range(ld):
            data_c = []
            for gene in common_genes:
                data_c.append((data[i])[geneNames[i].index(gene)])
            data_2.append(np.array(data_c)) 
        self. data = data_2     
        return self.data
    
    def normalized_data(self, normalized_number):
        """
        This function normalizes data with three normalization methods.
        """
        data = self.data[:]
        ld = self.ld
        data_norm = []
        if normalized_number == 0:
            for i in range(ld):
                data_norm.append(sd.get_normalization(data[i])[0])
        if normalized_number == 1:
            for i in range(ld):
                data_norm.append(sd.get_normalization(data[i])[1])
        if normalized_number == 2:
            for i in range(ld):
                data_norm.append(sd.get_normalization(data[i])[2])                
        self.data = data_norm 
        return self.data
    
    def bootstrap(self, idx):
        """This function chooses idx number of genes randomely. 
        """
        data = self.data[:]
        common_genes = self.common_genes[:]
        ld = self.ld
        np.random.seed(1234)
        idx_BS = np.random.choice(np.shape(data[0])[0], idx)
        data_BS = []
        for i in range(ld):
            data_BS.append((data[i])[idx_BS,:])
        self.data = data_BS
        self.idx_BS = idx_BS
        geneNames_BS = []
        for j in idx_BS:
            geneNames_BS.append(common_genes[j])
        self.geneNames_BS = geneNames_BS
        return self.data
    
    def Correlation(self, g = 0, name = None, data_name =None, g_samples = None, c_samples = None, bootsNumbers = None, BS = False, save = False):
        """
        This function computes gene-gene correlation and cell-cell correlation
        for a list of data where each element of the list is a data-point expression matrix.
        For example, ERY_data = [ERY_D0, ERY_D1, ERY_D3, ERY_D6] is a list of 4 matrices, and the 
        correlation function compute gene-gene correlation for each of them (ERY_D0, ERY_D1,
        ERY_D3 and ERY_D6). g is a binary paramter where g = 0  
        implies no p-value filtering and g = 1 does apply a p_value filter.
        input: 
            g: determines the use of the p-value filter 
            name: Name of cell type
            Data_name: Name of data
            g_samples: the number of genes which are suppose to be bootstrapped
            c_samples: The number of cells which should be bootstrapped
            bootsNumbers: the number of bootstrappings
        Output:
            data_gg_cor: it is a list of gene-gene correlation matrices
            data_cc_cor: it is a list of cell-cell correlation matrices
        """
        data = self.data[:]
        print(data[0].shape)
        ld = self.ld 
        if BS == True:
            data_gg_cor = []
            data_gg_cor_avg = []

            data_cc_cor = []
            data_cc_cor_avg = []

            for i in range(bootsNumbers):
                print('boot')
                print(i)
                bs_ind_g = np.random.choice(np.shape(data[0])[0], g_samples)
                bs_ind_c = np.random.choice(np.shape(data[0])[1], c_samples)
                bs_data_gg_cor = []
                bs_data_gg_cor_avg = []

                bs_data_cc_cor = []
                bs_data_cc_cor_avg = []

                for t in range(ld):
                    data_bs_g = (data[t])[bs_ind_g,:]
                    data_bs = (data_bs_g)[:,bs_ind_c]

                    gg_cor = GF.get_corr(data_bs, g)
                    cc_cor = GF.get_corr(np.transpose(data_bs),0)
                    gg_avg = GF.avg(gg_cor)[0]
                    cc_avg = GF.avg(cc_cor)[0]

                    bs_data_gg_cor.append(gg_cor)
                    bs_data_cc_cor.append(cc_cor)
                    bs_data_gg_cor_avg.append(gg_avg)
                    bs_data_cc_cor_avg.append(cc_avg)

                    
                data_gg_cor.append(bs_data_gg_cor)
                data_gg_cor_avg.append(bs_data_gg_cor_avg)

                data_cc_cor.append(bs_data_cc_cor)
                data_cc_cor_avg.append(bs_data_cc_cor_avg)

        else:
            data_gg_cor_avg = []
            data_cc_cor_avg = []
            for i in range(ld):
                data_gg_cor_avg.append(GF.get_corr(data[i], g))
                data_cc_cor_avg.append(GF.get_corr(np.transpose(data[i]), 0))
  
        if save == True :        
            np.save('data/correlation/data_gg_cor_'+str(name)+str('_')+str(data_name), np.array(data_gg_cor))  
            np.save('data/correlation/data_gg_cor_avg_'+str(name)+str('_')+str(data_name), np.array(data_gg_cor_avg)) 

            np.save('data/correlation/data_cc_cor_'+str(name)+str('_')+str(data_name), np.array(data_cc_cor))
            np.save('data/correlation/data_cc_cor_avg_'+str(name)+str('_')+str(data_name), np.array(data_cc_cor_avg))

       
        return data_gg_cor_avg, data_cc_cor_avg
    

    
                
    
    def binarization(self):#
        """
        This function binarizes data by an on-off binarization or median binarization approach, respectively.
         output:     
            data_binary_1 or data_binary_2
        """
        data = self.data[:]
        ld = self.ld
        data_binary_1 = []
        data_binary_2 = []
       # data_binary_3 = []
        data_merg = np.concatenate(data, axis=1)
        for i in range(ld):
            data_binary_1.append(GF.get_On_Off_binary(data[i]))
            data_binary_2.append(GF.get_binary_median_2(data[i], data_merg))
          #  data_binary_3.append(GF.get_binary_median_3(data[i]))#, data[i]))
        self.data_binary_1 = data_binary_1
        self.data_binary_2 = data_binary_2   
       # self.data_binary_3 = data_binary_3
        return self.data_binary_1, self.data_binary_2#,  self.data_binary_3           
        self.data_binary_1 = data_binary_1
        self.data_binary_2 = data_binary_2   
        return self.data_binary_1, self.data_binary_2         
        
    
    def Mutual_information(self, g = 0, binary_type ='on_off', name = None, data_name =None, g_samples = None, c_samples = None, bootsNumbers = None, BS = False, save = False):
        """
        This function computes a gene-gene mutual information matrix and a cell-cell
        mutual_information matrix for a list of data where each element of the 
        list is a data-poit expression matrix. For example, for 
        ERY_data = [ERY_D0, ERY_D1, ERY_D3, ERY_D6], the 
        Mutual_information function computes the gene-gene mutual_information 
        for ERY_D0, ERY_D1, ERY_D3 and ERY_D6. g is a paramter that determines
        is p-value filtering is applied (g = 1) or not (g = 0).  To apply mutual information, we need to
        know the probability distribution of the data. For this purpose, we binarize the data and use Bernoulli distribution.
        This can be done with the use of either on-off binarization or median binarization.
        input: 
            g: [0: No, 1: yes] determine use p-value filter or not
            binary_type: ['on_off', 'median_all', 'median']
        Output:
            data_gg_MI: it is a list of gene-gene mutual_information matrices.
            data_cc_MI: it is al ist of cell-cell mutual_information matrices.
        """
        ld = self.ld
        if binary_type == 'on_off':
            data_binary = self.data_binary_1[:]
        if binary_type == 'median_all':
            data_binary = self.data_binary_2[:]
            
            
        if BS == True:
            data_gg_MI = []
            data_gg_MI_avg = []

            data_cc_MI = []
            data_cc_MI_avg = []

            for i in range(bootsNumbers):
                print('boot')
                print(i)

                bs_ind_g = np.random.choice(np.shape(data_binary[0])[0], g_samples)
                bs_ind_c = np.random.choice(np.shape(data_binary[0])[1], c_samples)
                bs_data_gg_MI = []
                bs_data_gg_MI_avg = []

                bs_data_cc_MI = []
                bs_data_cc_MI_avg = []

                for t in range(ld):
                    data_bs_g = (data_binary[t])[bs_ind_g, :]
                    data_bs = (data_bs_g)[:, bs_ind_c]
                    gg_MI = GF.MI_2(data_bs, g)
                    gg_avg = GF.avg(gg_MI)[0]
                    
                    cc_MI = GF.MI_2(np.transpose(data_bs), g)
                    cc_avg = GF.avg(cc_MI)[0]
                    
                    bs_data_gg_MI.append(gg_MI)
                    bs_data_gg_MI_avg.append(gg_avg)
                    
                    bs_data_cc_MI.append(cc_MI)
                    bs_data_cc_MI_avg.append(cc_avg)
                    
                data_gg_MI.append(bs_data_gg_MI)
                data_gg_MI_avg.append(bs_data_gg_MI_avg)
                
                data_cc_MI.append(bs_data_cc_MI)
                data_cc_MI_avg.append(bs_data_cc_MI_avg)

        else:
            data_gg_MI_avg = []
            
            data_cc_MI_avg = []
            for i in range(ld):
                data_gg_MI_avg.append(GF.MI_2(data_binary[i], g))
                data_cc_MI_avg.append(GF.MI_2(np.transpose(data_binary[i]), g))


        if save == True : 
            np.save('data/MI/data_gg_MI_'+str(name)+str('_')+str(data_name)+str('_')+str(binary_type), np.array(data_gg_MI))
            np.save('data/MI/data_gg_MI_avg_'+str(name)+str('_')+str(data_name)+str('_')+str(binary_type), np.array(data_gg_MI_avg))

            np.save('data/MI/data_cc_MI_'+str(name)+str('_')+str(data_name)+str('_')+str(binary_type), np.array(data_cc_MI))
            np.save('data/MI/data_cc_MI_avg_'+str(name)+str('_')+str(data_name)+str('_')+str(binary_type), np.array(data_cc_MI_avg))


        return data_gg_MI_avg, data_cc_MI_avg      

    
        
    def Index(self, gg_data, cc_data, BS = False):
        """
        By getting the avarge on the entries of an array or a 
        matrix, thsi function computes the avrage gg or cc matrices and then 
        computes critical indices for correlation matrices or mutual information
        matrices by dividing these.
        input:
            gg_data: gene-gene matrix list
            cc_data: cell-cell matrix list
        output:
            gg_avg: gene-gene avrage list for a list of gene-gene matrices list
            cc_avg: cell-cell avrage list for a list of cell-cell matrices list  
        """
        ld = self.ld
        if BS == True:
            gg_avg = np.mean(gg_data, axis=0)
            cc_avg = np.mean(cc_data, axis=0)
        else: 
            gg_avg = []
            cc_avg = []
            for i in range(ld):
                gg_avg.append(np.mean(abs(gg_data[i])))
                cc_avg.append(np.mean(abs(cc_data[i])))
        gg_avg = np.array(gg_avg)
        cc_avg = np.array(cc_avg)        
        
        Idx = gg_avg/cc_avg
        
        return gg_avg, cc_avg, Idx
    


    
    def EB_SEM(self, data_1, data_2, data_3, data_4, boots = False):
        """
        This function computes the error bar for a list of data.
        If the data is a matrix like data_1, it uses the Standard Error of the Mean, if it is
        an array of indices it applies propagation of uncertainty
        input:
            data_1: gg-data 
            data_2: cc-data
            data_3: gg_avg
            data_4: cc_avg
        output:
            EB_data_1: error bar of gg-avg
            EB_data_2: error bar of cc-avg
            EB_idx: error bar of critical idx
        """
        ld = self.ld
        if boots == True:
            EB_data_1 = np.std(data_3, axis=0)/math.sqrt(len(data_3))
            EB_data_2 = np.std(data_4, axis=0)/math.sqrt(len(data_4))
        else:
            EB_data_1 = []
            EB_data_2 = []
            for i in range(ld):
                 EB_data_1.append(np.std(data_3[i])/math.sqrt(np.shape(data_3[i])[0]*np.shape(data_3[i])[1]))
                 EB_data_2.append(np.std(data_4[i])/math.sqrt(np.shape(data_3[i])[0]*np.shape(data_3[i])[1]))
            
        EB_idx = GF.get_SEM(data_1, data_2, EB_data_1, EB_data_2)
       
            
        return EB_data_1, EB_data_2, EB_idx   
             
             
        
    def Plot_index(self, listData, saveAs, listColor, listLabels, xLabel, yLabel, listOfSteps, currentNamesList, ytik, legend_loc,lsize, EB):
        """
        This function plots a list of data. 
        input:
            listData:  a list of data which each element is a list of indices
            saveAs: the name of figure which will be saved by this name
            listColor: list of colors which are costumized
            listLabels: is the list of the legend for the figures
            xLabel: the x axis label
            yLabel: the y axis label
            titel: is the title of the figure
            listofSteps: is the list of steps on x-axis
            currentNamesList: is the name of list steps on x-axis
            ESM: is the error band for listData  
        """
      
        
        fig, ax = plt.subplots()
        ax.tick_params(axis='both', which='major', labelsize=20)
        plt.rc('legend',fontsize=lsize)
        plt.xlabel(xLabel, fontsize=25)
        plt.ylabel(yLabel , fontsize=25)
        #plt.title(title, fontsize=15)
        for i in range(len(listData)):
            plt.plot(listOfSteps, listData[i], label=listLabels[i], color=listColor[i], linewidth =3)
            ax.errorbar(listOfSteps, listData[i], yerr = EB[i], color=listColor[i], alpha=0.9, capsize=8,elinewidth=5,  markeredgewidth=2)#, fmt='o'
        plt.legend(loc= legend_loc)
        plt.ylim((0))

        ax.set_xticklabels(currentNamesList)#, fontsize=20)
        ax.set_yticks(ytik)
        plt.tight_layout()
        plt.show()
        plt.savefig(os.path.join(r'data/index_plots',saveAs))
        
        return
    
    
    def gibbsEnergy(self, binary_type, dist, name = None, data_name =None, g_samples = None, c_samples = None, boots = None, BS = False, save = False):
        """
        This function computes the KulBack-Leibler distance between two distributions.
        This computation uses the distribution of each gene over the 
        cells and determined its distance to a refrence distribution,
        which is chosen here to be the uniform distibution.
        input: 
            binary_type: [None: without binarization, on-off, median]
            geneNames_BS: The list of genes 
            BS: [True: use bootstraping, False: do not use bootstaping]
            dist: the distribution on the data ['Norm': normal distribution,
            'binary': binary distribution]
        output:
            KL_List: a matrix inovlves KL for whole of the genes in each data_point
            KL_List: average of KL distance
        """
        data = self.data[:]
        ld = self.ld
        if binary_type == None: 
            data_binary = data
            
        if binary_type == 'on_off':
            data_binary = self.data_binary_1[:]
        if binary_type == 'median_all':
            data_binary = self.data_binary_2[:]  
        if BS == True:
            geneNames = self.common_genes[:]
            KL_List = []
            for t in range(ld):
                data_1 = data_binary[t]
                print('t')
                print((data_1))
                print('___')
                KL_List_1 = []
                for i in range(boots):
                    print('boot')
                    print(i)
                    bs_ind_g = np.random.choice(np.shape(data_1)[0], g_samples)
                    bs_ind_c = np.random.choice(np.shape(data_1)[1], c_samples)
                    data_bs_g_0 = (data_binary[0])[bs_ind_g,:]
                    data_bs_0 = (data_bs_g_0)[:,bs_ind_c]
                    geneNames_bs = []
                    for j in bs_ind_g:
                        geneNames_bs.append(geneNames[j])
                    data_bs_g = (data_1)[bs_ind_g,:]
                    data_bs = (data_bs_g)[:,bs_ind_c]
                    KL_data = []
                    for gene in geneNames_bs:
                        KL_data.append(GF.gibbs(gene, geneNames_bs, data_bs_0, data_bs, dist)) 
                        
                    KL_List_1.append(np.array(KL_data))
                KL_List.append(np.mean(np.array(KL_List_1),axis=0))
        else: 
            KL_List = [] 
            geneNames = self.common_genes[:]
            #KL_indx = []
            for t in range(ld):
                KL_data = []
                for gene in geneNames:
                    KL_data.append(GF.gibbs(gene, geneNames, data_binary[0], data_binary[t],dist))  
                KL_List.append(np.array(KL_data))
         
        if save == True :
            np.save('data/KL/KL_List_'+str(name)+str('_')+str(data_name)+str('_')+str(binary_type), np.array(KL_List))


        return np.array(KL_List)#, KL_indx
        
  
            
            
    def high_expressed_genes(self, data_1, data_2):
        """
        This function finds genes with high expression values in two input data sets.
        """
        d1 = np.concatenate(data_1, axis =1)
        d2 = np.concatenate(data_2, axis =1)
        d = np.concatenate((d1,d2), axis =1)
        
        ncz = np.count_nonzero(d, axis=1)
        s_indx = ((-ncz).argsort()) 
        
        return s_indx    
            
    
    def plot_hist_KL(self, Data,  c, d, cellType, box, xlabel, ylabel, ylim, xlim, xtik, ytik, saveAs, bins = None, hist = False):#, bins = None,  x_labele = "", y_lable = "", title = "", ylim = [10,10], xlim = [10,10]):
        
        """
        This function plots the histogram the input data.
        """
        
        if hist == True:
            for i in range(len(Data)):
                fig, ax = plt.subplots()
                ax.tick_params(axis='both', which='major', labelsize=20)
                ax.hist(Data[i], bins, color=c)
                ax.set_xlabel(xlabel, fontsize = 25)
                ax.set_ylabel(ylabel, fontsize = 25)
                ax.set_ylim(ylim)
                ax.set_xlim(xlim)
                ax.set_xticks(xtik)
                ax.set_yticks(ytik)
                plt.show()
               # ax.set_title('Day '+str(d[i]), fontsize = 12)
                plt.tight_layout()
                plt.savefig(saveAs+str(cellType)+str('_')+str(box)+str('_')+'Day'+str('_')+str(d[i])+'.pdf')
        else:
            for i in range(len(Data)):
                fig, ax = plt.subplots()
                ax.tick_params(axis='both', which='major', labelsize=22)
                ax.plot(Data[i], color=c, linewidth=3)
                ax.set_xlabel(xlabel, fontsize = 25)
                ax.set_ylabel(ylabel, fontsize = 25)
                ax.set_ylim(ylim)
                ax.set_xlim(xlim)
                ax.set_yticks(ytik)
                ax.set_xticks(xtik)
                plt.show()
               # ax.set_title('Day '+str(d[i]), fontsize = 12)
                plt.tight_layout()
                plt.savefig(saveAs+str(cellType)+str('_')+str(box)+str('_')+'Day'+str('_')+str(d[i])+'.pdf')

           
    def scatter_plot(self,Data, genes, y, c, ylim, d, cellType, box, xlabel, ylabel, xtik, ytik, saveAs):
             """
             This function plots the scatter plot of data per each gene
             """
             for i in range(len(Data)):
                   
                   fig, ax = plt.subplots()#figsize=(8,4))
                   ax.tick_params(axis='both', which='major', labelsize=20)

                   ax.plot(Data[i],color= c, linewidth=3)#,facecolors='none')
                   for j in y:
                       idx =genes[i].index(j)
                       xy = Data[i][idx]
                       plt.plot(idx, xy, color = 'b', marker='*', markersize=16,linewidth=2)
                   ax.set_xlabel(xlabel, fontsize = 25)
                   ax.set_ylabel(ylabel, fontsize = 25)
                   ax.set_ylim(ylim)
                   ax.set_yticks(ytik)
                   ax.set_xticks(xtik)
                   ax.set_xticklabels(genes[i], rotation =90)#, fontsize=20)
                   plt.tight_layout()
                   plt.savefig('data/index_plots/'+saveAs+str(cellType)+str('_')+str(box)+str('_')+'Day'+str('_')+str(d[i])+'.pdf')
            
           
    def heatmap_plot(self, Data, c, d, cellType, box, xlabel, ylabel, title, saveAs):
        """
        This function plots the heatmap
        c: 
        d: day index per time 
        cellType: represent cell type, mutant or control or A or B
        """

        ld = self.ld 
        for i in range(ld):
             fig, ax = plt.subplots()
             heatmap = ax.pcolor(Data[i], cmap = plt.get_cmap('jet')) # RdGy_r
             cbar =fig.colorbar(heatmap)
             ax.invert_yaxis()
             plt.title(title +'Day '+str(d[i]), fontsize=25)
             plt.xlabel(xlabel, fontsize=25)
             plt.ylabel(ylabel , fontsize=25)
             cbar.set_label(c, fontsize=25)
             plt.tight_layout()
             plt.show()
             plt.savefig('data/plots/heatmaps/'+saveAs+str(cellType)+str('_')+str(box)+str('_')+'Day'+str('_')+str(d[i])+'.pdf')


        
        
