#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code first bi-clusters the data for subsequent plotting in Matlab (Subfigure A of Figure 2 in the manuscript).
For cell types of both mutant (A) and Control (B), the KL values are calculated and then the corresponding distributions are plotted (Subfigure A and B of Figure 4 in the manuscript).
For this purpose, the 2000 genes with the highest gene expressions values are filterd.

Created on Tue Aug 10 17:11:25 2021

@author: susanghaderi
"""

import numpy as np
from Criticl_index_computation import CriticalIndex
import matplotlib.pyplot as plt
import os
#%%
A_cut_cell  = np.load(os.path.join('data/IPS_data', 'A_cut_cell.npy'), allow_pickle=True)
B_cut_cell  = np.load(os.path.join('data/IPS_data', 'B_cut_cell.npy'), allow_pickle=True)
A_geneNames = np.load(os.path.join('data/IPS_data', 'A_geneNames.npy'), allow_pickle=True)
B_geneNames = np.load(os.path.join('data/IPS_data', 'B_geneNames.npy'), allow_pickle=True)


#%%

"""
    To use the CriticalIndex class, an objact class for each 
    cell type should be constructed. For example, there should be one object for A and on for B like e.g.,
    A_ob and B_obj.
    The input for each object is the cell matrix and the corresponding gene Names.
"""       
A_obj = CriticalIndex(A_cut_cell, A_geneNames) 
    
""" Determining the common gene names between A and B """
common_genes = A_obj.com_genes(A_geneNames, B_geneNames)
   
"""Final data based on common genes."""
A_data_final = A_obj.final_data()
    
""" Normalization of data - can be comment if not needed."""
A_data_norm = A_obj.normalized_data(0)
    
    
B_obj = CriticalIndex(B_cut_cell, B_geneNames) 
    
""" Common gene names between A and B """
common_genes = B_obj.com_genes(B_geneNames, A_geneNames)
   
"""Final data based on common genes."""
B_data_final = B_obj.final_data()
    
""" Normalization of data - can be comment if not needed"""
B_data_norm = B_obj.normalized_data(0)
    

    
""" Binarization of data"""
data_binary_1_A, data_binary_2_A = A_obj.binarization()
data_binary_1_B, data_binary_2_B = B_obj.binarization()

#%% bi-clustring
"""Data for bi-clustring for Figure 1A and subsequent plotting in Matlab"""
from sklearn.cluster import SpectralBiclustering
import  scipy.io

biClustring = 1

if biClustring == 1:
   A_all_g = np.concatenate(A_data_final, axis=1)
   B_all_g = np.concatenate(B_data_final, axis=1)

   A_all_c = np.concatenate(A_data_final, axis=0)
   B_all_c = np.concatenate(B_data_final, axis=0)

   nz_A_gene = np.count_nonzero(A_all_g, axis=1)
   nz_A_cell = np.count_nonzero(A_all_c, axis=0)
   """Subfigure A from figure 1 for bi-clustring metaphore"""
    
   cell_indx_A =(-nz_A_cell).argsort()[0:150]
   gene_indx_A =(-nz_A_gene).argsort()[0:200]

   cell_indx_A.sort()
   gene_indx_A.sort()

   A_cut =[]

   for i in range(len(A_data_final)):
       A_cut.append(A_data_final[i][gene_indx_A][:,cell_indx_A])

       n_clusters = (4, 4)
       d = [0,10,14,42]
       bi_data =[]
   for i in range(len(A_cut)):
       data = A_cut[i]
       model = SpectralBiclustering(n_clusters=n_clusters, method='log',
                              random_state=0)
       model.fit(data)

       fit_data = data[np.argsort(model.row_labels_)]
       fit_data = fit_data[:, np.argsort(model.column_labels_)]
       bi_data.append(fit_data)

   scipy.io.savemat(os.path.join('data/bi_clustring','D0.mat'), mdict={'D0': bi_data[0]})
   scipy.io.savemat(os.path.join('data/bi_clustring','D10.mat'), mdict={'D10': bi_data[1]})
   scipy.io.savemat(os.path.join('data/bi_clustring','D14.mat'), mdict={'D14': bi_data[2]})
   scipy.io.savemat(os.path.join('data/bi_clustring','D42.mat'), mdict={'D42': bi_data[3]})

"""Subsequent plotting of the surface in Matlab"""

#%%                         
"""
step 0: computing KL values
"""

Gibbs_energy = 1
if Gibbs_energy == 1:
    """Concatenate A day 0 and day 42 and compute KL for them. After subsequent ordering,
    the 80% highest indices are kept (13,000 genes)
    """ 
    AB_0 = np.concatenate((A_data_norm[0],B_data_norm[0]), axis = 1)
    AB_42 = np.concatenate((A_data_norm[3],B_data_norm[3]), axis = 1)
      
   
    AB_042 = np.concatenate((AB_0, AB_42), axis= 1)  
    AB_042_obj = CriticalIndex(AB_042, common_genes)
    common_genes_AB = AB_042_obj.com_genes(A_geneNames, B_geneNames)


    nz = np.count_nonzero(AB_042, axis=1)
    sorted_nz = np.argsort(-nz)
    s = sorted_nz[0:4000]

    cut_genes = []
    for g in s:
        cut_genes.append(common_genes_AB[g])
    s = sorted(s)
    
    A_new = []
    B_new = []
    for i in range(len(A_data_norm)):
         A_new.append((A_data_norm[i])[s])
         B_new.append((B_data_norm[i])[s])
    
    AB_genes = [cut_genes, cut_genes]
    A_new_obj = CriticalIndex(A_new, AB_genes)  
    common_genes_AB = A_new_obj.com_genes(AB_genes)
    KL_A_gene = A_new_obj.gibbsEnergy(None, 'Normal', 'A', 'Jonas')
    
    B_new_obj = CriticalIndex(B_new, AB_genes)
    common_genes_AB = B_new_obj.com_genes(AB_genes)
    KL_B_gene = B_new_obj.gibbsEnergy(None, 'Normal', 'B', 'Jonas')
    
    np.save(os.path.join('data/KL','KL_new_A.npy'), KL_A_gene)
    np.save(os.path.join('data/KL','KL_new_B.npy'), KL_B_gene)
    np.save(os.path.join('data/KL','new_AB_gene.npy'), common_genes_AB)
     
#%%
"""
Filtering KL values less than 5 to get rid of outliers.
"""

KL_A = []
KL_B = []

for i in range(len(KL_B_gene)):

    KL_A_5 = []
    KL_B_5 = []
    for t in range(len(KL_A_gene[i])):
        if KL_A_gene[i][t]<= 5:
            KL_A_5.append(KL_A_gene[i][t])
            
        if KL_B_gene[i][t]<= 5:
            KL_B_5.append(KL_B_gene[i][t])
            

    KL_A.append(KL_A_5)
    KL_B.append(KL_B_5)
    
#%%

"""
Subfigures A and B,in Figure 4:
Plotting the overlap curves of the mutant data between day 0 to day 42
"""

from scipy.stats import norm
label_1 = [r'Day 0', r'Day 10', r'Day 14', r'Day 42']
color_1=['green','#e66101','black','#5e3c99']

fig, ax = plt.subplots()
for i in range(len(KL_A)):
    x= np.linspace(np.min(KL_A[i]),np.max(KL_A[i]),1000)
    mean,std=norm.fit(KL_A[i])
    y = norm.pdf(x, mean, std)
    ax.plot(x, y,label=label_1[i], color=color_1[i])
    plt.show()
plt.xlabel('KL  values (Mutant)', fontsize=15)
plt.ylabel('Probability', fontsize=15)
ax.legend(loc='upper right', fontsize=15, frameon=False)
plt.savefig(os.path.join('data/plots','KL_values_normal_dist_A.pdf'))

fig, ax = plt.subplots()
for i in range(len(KL_B)):
    x= np.linspace(np.min(KL_B[i]),np.max(KL_B[i]),1000)
    mean,std=norm.fit(KL_B[i])
    y = norm.pdf(x, mean, std)
    ax.plot(x, y,label=label_1[i], color=color_1[i])
    plt.show()
plt.xlabel('KL  values (Control)', fontsize=15)
plt.ylabel('Probability', fontsize=15)
ax.legend(loc='upper right', fontsize=15, frameon=False)
plt.savefig(os.path.join('data/plots','KL_values_normal_dist_B.pdf'))
#%%    
