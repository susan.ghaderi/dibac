#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code applies neural network self-organizing map (SOM) to classify and cluster KL values.   Finally, the
neurons are plotted as shown in Figure 4C of the ms.

One thing to note before running SOM: Variables measured in different
units could interfere with the speed and accuracy of our analysis.
To prevent any variable 
from overpowering the others, we need to standardize all variables. 
Standardization is analogous to expressing each variable in terms of 
percentiles, meaning to shift them onto a uniform standard scale, 
so that they are of the same measurement unit.

Created on Tue Aug 10 09:55:11 2021

@author: susanghaderi
"""



import numpy as np
from matplotlib import pyplot as plt
from sompy.sompy import SOMFactory
from mpl_toolkits.axes_grid1 import AxesGrid
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
#%%
"""
Step 1: We load the KL values from the directory
"""

A = np.load('data/KL'+'/KL_new_A.npy')
data_A = A
names_A = ['A Day 0', 'A Day 10', 'A Day 14', 'A Day 42']

B = np.load('data/KL'+'/KL_new_B.npy')
data_B = B
names_B = ['B Day 0', 'B Day 10', 'B Day 14', 'B Day42']

genes_names  = np.load('data/KL'+'/new_AB_gene.npy')


 #%% 
"""
Step 2: Merging A and B into matrix C and compute 
its transpose including the names
"""

C=[A[0],A[1],A[2],A[3],B[0],B[1],B[2],B[3]]
C = np.transpose(C)
names_c =['Mutant Day 0', 'Mutant Day 10', 'Mutant Day 14', 'Mutant Day 42',  'Control Day 0','Control Day 10','Control Day 14', 'Control Day 42']
#%%
"""
Step 3: Creation of a class "sm3" to do required operations on C (data),
Dimension reduction method is set to "pca", and neurons are shown by hexgons ("hexa")
"""
m =60
sm = SOMFactory().build(C, mapsize = [m,m], normalization = 'var', initialization ='pca', component_names = '', lattice="hexa")
sm.train(n_job=1, verbose = False, train_rough_len =100, train_finetune_len =5)

#%%
M = sm.codebook.matrix

fig = plt.figure(figsize=(8, 10))
j=0
i=0
grid = AxesGrid(fig, 111,
                nrows_ncols=(2, 4),
                axes_pad=0.05,
                cbar_mode='single',
                cbar_location='right',
                cbar_pad=0.1
                )

label_1 = [r'Day 0', r'Day 10', r'Day 14', r'Day 42',r'Day 0', r'Day 10', r'Day 14', r'Day 42']
for ax in grid:

    if i<4:
        #ax.set_axis_off()
        print(f'i={i}')
        im = ax.pcolor(M[:,i].reshape(m,m), cmap='viridis')
        ax.set_xticks([])
        ax.set_yticks([])
        if i==0:
            ax.set_ylabel('Mutant',fontsize=12)
            
    if i>=4:
        print(f'i={i}')
        im = ax.pcolor(M[:,i].reshape(m,m),cmap='viridis')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel(label_1[i],fontsize=12)
        if i==4:
            
            ax.set_ylabel('Control',fontsize=12)


    i+=1 
plt.setp(ax.get_xticklabels(), visible=False)
cbar = ax.cax.colorbar(im)
cbar = grid.cbar_axes[0].colorbar(im)
cbar.set_label_text('KL values',fontsize=12)
for t in cbar.ax.get_yticklabels():
     t.set_fontsize(7)
fig = plt.gcf()
plt.savefig("data/plots/SOM_component_60_rectan.pdf", bbox_inches='tight')
plt.show()


#%%

"""
Plotting the SOM of bum of KL-values as 3-D plots. 
"""
M = sm.codebook.matrix
X1 , X2 = np.meshgrid(np.arange(m),np.arange(m))

for i in range(8):
    fig = plt.figure()
    axs = fig.add_subplot(111, projection='3d')
    surf= axs.plot_surface(X1,X2, M[:,i].reshape(m,m),rstride=1, cstride=1,cmap='viridis')
    axs.set_xlabel('Gene state')
    axs.set_ylabel('Gene state')    
    axs.set_title(names_c[i])
    plt.savefig(f'data/plots/SOM_component_{i}.png')



p1 = sm.project_data(C)

xy_ind = sm.bmu_ind_to_xy(p1)  


#%%
M = sm.codebook.matrix
p1 = sm.project_data(C)

xy_ind = sm.bmu_ind_to_xy(p1)  


"""
This part of the code recognizes the high  KL values and corresponding gene names 
 per each day based on the SOM graph
For example at day_1, the KL values were cut off with bum>0.8 as 
high values and for day 42, with bum>1.2.

"""
d_0 =[]
d_10 =[]
d_14 =[]
d_42 =[]
d_0_b =[]
d_10_b =[]
d_14_b =[]
d_42_b =[]

for i in range(M[:,0].shape[0]):
    if M[i,0]>3:
        d_0.append(i)
    if M[i,1]>1.5:
        d_10.append(i)
    if M[i,2]>1.75:
        d_14.append(i)
    if M[i,3]>3:
        d_42.append(i)
    if M[i,4]>3:
        d_0_b.append(i)
    if M[i,5]>2:
        d_10_b.append(i)
    if M[i,6]>2.5:
        d_14_b.append(i)
    if M[i,7]>2.75:
        d_42_b.append(i)

v = set(list(p1))     
#%%
"""
we have the position of each bmu in the gene location in p1. 
for example if bum
of 3400 is associated with gene 1800. Since every location
 in the map should be associated with a gene value, for each 
 position from M matrix, first we find those genes which are
 located at that location, it is called l1. then we find those 
 gene names index in the gene list and put them in g_list. 
"""
l = []
g_list = []
for j in range(m*m):
    l1=([i for i in range(len(p1)) if p1[i]==j])
    g_list.append(genes_names[l1])
    l.append(l1)
    
#%%
x= xy_ind
ind=[]
g_ind_A0 =[]
for i in range(x.shape[0]):
    if (54<=x[i,:][0]<=59)& (x[i,:][1]==55):
        ind.append(i)
        g_ind_A0.append(x[i][2])
        
    if (52<=x[i,:][0]<=59)& (x[i,:][1]==56):
        ind.append(i)
        g_ind_A0.append(x[i][2])

    if (50<=x[i,:][0]<=59)& (x[i,:][1]==57):
        ind.append(i)
        g_ind_A0.append(x[i][2])

    if (49<=x[i,:][0]<=59)& (x[i,:][1]==58):
        ind.append(i)
        g_ind_A0.append(x[i][2])

    if (46<=x[i,:][0]<=59)& (x[i,:][1]==59):
        ind.append(i)
        g_ind_A0.append(x[i][2])
        
g_ind_A0 = set(g_ind_A0)
g_A0 = []
KL_A0 = []
for t in g_ind_A0:
    g_A0 += list(g_list[t])
    KL_A0 += list(l[t])
#%%
        
with open('data/SOM'+ "/A_0_high_SOM.txt", 'w') as output:
    for row in g_A0:
        output.write(str(row) + '\n')

#%%
x= xy_ind
ind=[]
g_ind_A42_1 =[]
for i in range(x.shape[0]):
    if (49<=x[i,:][0]<=59)& (0<=x[i,:][1]<=2):
        ind.append(i)
        g_ind_A42_1.append(x[i][2])
        
    if (50<=x[i,:][0]<=59)& (x[i,:][1]==3):
        ind.append(i)
        g_ind_A42_1.append(x[i][2])

    if (51<=x[i,:][0]<=59)& (x[i,:][1]==4):
        ind.append(i)
        g_ind_A42_1.append(x[i][2])
        
g_ind_A42_1 = set(g_ind_A42_1)
g_A42_1 = []
KL_A42_1 = []

for t in g_ind_A42_1:
    g_A42_1+=list(g_list[t])
    KL_A42_1 += list(l[t])

    
    
with open('data/SOM'+ "/A_42_1_high_SOM.txt", 'w') as output:
    for row in g_A42_1:
        output.write(str(row) + '\n')
    
#%%
x= xy_ind
ind=[]
g_ind_A42_2 =[]
for i in range(x.shape[0]):
    if (41<=x[i,:][0]<=47)& (0<=x[i,:][1]<=2):
        ind.append(i)
        g_ind_A42_2.append(x[i][2])

g_ind_A42_2 = set(g_ind_A42_2)
g_A42_2 = []
KL_A42_2 = []

for t in g_ind_A42_2:
    g_A42_2+=list(g_list[t])
    KL_A42_2 += list(l[t])
    
with open('data/SOM'+ "/A_42_2_high_SOM.txt", 'w') as output:
    for row in g_A42_2:
        output.write(str(row) + '\n')
    
#%%
x= xy_ind
ind=[]
g_ind_A42_3 =[]
for i in range(x.shape[0]):
    if (55<=x[i,:][0]<=58)& (x[i,:][1]==14):
        ind.append(i)
        g_ind_A42_3.append(x[i][2])
        
    if (54<=x[i,:][0]<=59)& (x[i,:][1]==15):
        ind.append(i)
        g_ind_A42_3.append(x[i][2])

    if (52<=x[i,:][0]<=59)& (x[i,:][1]==16):
        ind.append(i)
        g_ind_A42_3.append(x[i][2])
        
    if (50<=x[i,:][0]<=59)& (17<=x[i,:][1]<=21):
        ind.append(i)
        g_ind_A42_3.append(x[i][2])
    if (51<=x[i,:][0]<=55)& (x[i,:][1]==22):
        ind.append(i)
        g_ind_A42_3.append(x[i][2])
       
        
g_ind_A42_3 = set(g_ind_A42_3)
g_A42_3 = []
KL_A42_3 = []

for t in g_ind_A42_3:
    g_A42_3 += list(g_list[t])
    KL_A42_3 += list(l[t])
    
with open('data/SOM'+  "/A_42_3_high_SOM.txt", 'w') as output:
    for row in g_A42_3:
        output.write(str(row) + '\n')
    
#%%
ind=[]
g_ind_B0 =[]
for i in range(x.shape[0]):
    if (46<=x[i,:][0]<=59)& (57<=x[i,:][1]<=59):
        ind.append(i)
        g_ind_B0.append(x[i][2])
        
    if (48<=x[i,:][0]<=59)& (x[i,:][1]==56):
        ind.append(i)
        g_ind_B0.append(x[i][2])

    if (55<=x[i,:][0]<=59)& (x[i,:][1]==55):
        ind.append(i)
        g_ind_B0.append(x[i][2])
        
g_ind_B0 = set(g_ind_B0)
g_B0 = []
KL_B0 = []

for t in g_ind_B0:
    g_B0+=list(g_list[t])
    KL_B0 += list(l[t])

with open('data/SOM'+  "/B_0_high_SOM.txt", 'w') as output:
    for row in g_B0:
        output.write(str(row) + '\n')

#%%
ind=[]
g_ind_B42_1 =[]
for i in range(x.shape[0]):
    if (44<=x[i,:][0]<=59)& (x[i,:][1]==0):
        ind.append(i)
        g_ind_B42_1.append(x[i][2])
        
    if (45<=x[i,:][0]<=59)& (1<=x[i,:][1]==4):
        ind.append(i)
        g_ind_B42_1.append(x[i][2])

        
g_ind_B42_1 = set(g_ind_B42_1)
g_B42_1 = []
KL_B42_1 = []

for t in g_ind_B42_1:
    g_B42_1 += list(g_list[t])
    KL_B42_1 += list(l[t])
    
with open('data/SOM'+  "/B_42_1_high_SOM.txt", 'w') as output:
    for row in g_B42_1:
        output.write(str(row) + '\n')
    
#%%
ind=[]
g_ind_B42_2 =[]
for i in range(x.shape[0]):
    if (45<=x[i,:][0]<=59)& (5<=x[i,:][1]<=12):
        ind.append(i)
        g_ind_B42_2.append(x[i][2])
        

        
g_ind_B42_2 = set(g_ind_B42_2)
g_B42_2 = []
KL_B42_2 = []

for t in g_ind_B42_2:
    g_B42_2 += list(g_list[t])
    KL_B42_2 += list(l[t])
    
with open('data/SOM'+  "/B_42_2_high_SOM.txt", 'w') as output:
    for row in g_B42_2:
        output.write(str(row) + '\n')
    
#%%

ind=[]
g_ind_B42_3 =[]
for i in range(x.shape[0]):
    if (40<=x[i,:][0]<=59)& (13<=x[i,:][1]<=17):
        ind.append(i)
        g_ind_B42_3.append(x[i][2])
        

        
g_ind_B42_3 = set(g_ind_B42_3)
g_B42_3 = []
KL_B42_3 = []

for t in g_ind_B42_3:
    g_B42_3 += list(g_list[t])
    KL_B42_3 += list(l[t])
  
with open('data/SOM'+  "/B_42_3_high_SOM.txt", 'w') as output:
    for row in g_B42_3:
        output.write(str(row) + '\n')
    
#%%

ind=[]
g_ind_B42_4 =[]
for i in range(x.shape[0]):
    if (24<=x[i,:][0]<=31)& (9<=x[i,:][1]<=12):
        ind.append(i)
        g_ind_B42_4.append(x[i][2])
        

        
g_ind_B42_4 = set(g_ind_B42_4)
g_B42_4 = []
KL_B42_4 = []

for t in g_ind_B42_4:
    g_B42_4 += list(g_list[t])   
    KL_B42_4 += list(l[t])

with open('data/SOM'+  "/B_42_4_high_SOM.txt", 'w') as output:
    for row in g_B42_4:
        output.write(str(row) + '\n')
    
#%%
A_0_high_SOM     = pd.DataFrame(data ={'gene names': g_A0, 'KL values': A[0][KL_A0]})
A_42_1_high_SOM  = pd.DataFrame(data ={'gene names': g_A42_1, 'KL values': A[3][KL_A42_1]})
A_42_2_high_SOM  = pd.DataFrame(data ={'gene names': g_A42_2, 'KL values': A[3][KL_A42_2]})
A_42_3_high_SOM  = pd.DataFrame(data ={'gene names': g_A42_3, 'KL values': A[3][KL_A42_3]})

B_0_high_SOM     = pd.DataFrame(data ={'gene names': g_B0, 'KL values': B[0][KL_B0]})
B_42_1_high_SOM  = pd.DataFrame(data ={'gene names': g_B42_1, 'KL values': B[3][KL_B42_1]})
B_42_2_high_SOM  = pd.DataFrame(data ={'gene names': g_B42_2, 'KL values': B[3][KL_B42_2]})
B_42_3_high_SOM  = pd.DataFrame(data ={'gene names': g_B42_3, 'KL values': B[3][KL_B42_3]})
B_42_4_high_SOM  = pd.DataFrame(data ={'gene names': g_B42_4, 'KL values': B[3][KL_B42_4]})

  
    
A_0_high_SOM.to_csv('data/SOM'+  r'/A_0_high_SOM.txt')
A_42_1_high_SOM .to_csv('data/SOM'+ r'/A_42_1_high_SOM.txt')
A_42_2_high_SOM .to_csv('data/SOM'+  r'/A_42_2_high_SOM.txt')
A_42_3_high_SOM .to_csv('data/SOM'+  r'/A_42_3_high_SOM.txt')

B_0_high_SOM.to_csv('data/SOM'+ r'/B_0_high_SOM.txt')
B_42_1_high_SOM .to_csv('data/SOM'+  r'/B_42_1_high_SOM.txt')
B_42_2_high_SOM .to_csv('data/SOM'+ r'/B_42_2_high_SOM.txt')
B_42_3_high_SOM .to_csv('data/SOM'+ r'/B_42_3_high_SOM.txt')
B_42_4_high_SOM .to_csv('data/SOM'+  r'/B_42_4_high_SOM.txt')


#%%    
"""
Genes are mapped to hexagons of the map. With 60 neurons the length of g_list would be 3600. Therefore, from d_is the important genes are added to the days
"""    
A_0 = []
a_0 = []
for i in d_0:
   A_0 = A_0+list(g_list[i])
   a_0 = a_0+list(l[i])
   
A_10 = []
a_10 = []
for i in d_10:
   A_10 = A_10+list(g_list[i]) 
   a_10 = a_10+list(l[i])
   
A_14 = []
a_14 = [ ]
for i in d_14:
   A_14 = A_14+list(g_list[i])   
   a_14 = a_14+list(l[i])
    
A_42 = []
a_42 = []
for i in d_42:
   A_42 = A_42+list(g_list[i])
   a_42 = a_42+list(l[i])
   
   
B_0 = []
b_0 = []
for i in d_0_b:
   B_0 = B_0+list(g_list[i])
   b_0 = b_0+list(l[i])
 
B_10 = []
b_10 = []
for i in d_10_b:
   B_10 = B_10+list(g_list[i])   
   b_10 = b_10+list(l[i])
    
   
B_14 = []
b_14 = []
for i in d_14_b:
   B_14 = B_14+list(g_list[i]) 
   b_14 = b_14+list(l[i])
   
    
B_42 = []
b_42 = []
for i in d_42_b:
   B_42 = B_42 + list(g_list[i])
   b_42 = b_42 + list(l[i])

#%%
A_0_SOM  = pd.DataFrame(data ={'gene names': A_0, 'KL values': A[0][a_0]})
A_10_SOM  = pd.DataFrame(data ={'gene names': A_10, 'KL values': A[1][a_10]})
A_14_SOM  = pd.DataFrame(data ={'gene names': A_14, 'KL values': A[2][a_14]})
A_42_SOM  = pd.DataFrame(data ={'gene names': A_42, 'KL values': A[3][a_42]})
                               
B_0_SOM  = pd.DataFrame(data ={'gene names': B_0, 'KL values': B[0][b_0]})
B_10_SOM  = pd.DataFrame(data ={'gene names': B_10, 'KL values': B[1][b_10]})
B_14_SOM  = pd.DataFrame(data ={'gene names': B_14, 'KL values': B[2][b_14]})
B_42_SOM  = pd.DataFrame(data ={'gene names': B_42, 'KL values': B[3][b_42]})
                               
   
 
A_0_SOM.to_csv('data/SOM'+ r'/A_SOM_0_high.txt')
A_10_SOM.to_csv('data/SOM'+ r'/A_SOM_10_high.txt')
A_14_SOM.to_csv('data/SOM'+ r'/A_SOM_14_high.txt')
A_42_SOM.to_csv('data/SOM'+ r'/A_SOM_42_high.txt')


B_0_SOM.to_csv('data/SOM'+ r'/B_SOM_0_high.txt')
B_10_SOM.to_csv('data/SOM'+ r'/B_SOM_10_high.txt')
B_14_SOM.to_csv('data/SOM'+ r'/B_SOM_14_high.txt')
B_42_SOM.to_csv('data/SOM'+ r'/B_SOM_42_high.txt')
#%%
genes = genes_names
with open('data/SOM'+ "/gene.txt", 'w') as output:
    for row in genes:
        output.write(str(row) + '\n')


with open('data/SOM'+ "/g_0_a.txt", 'w') as output:
    for row in A_0:
        output.write(str(row) + '\n')
        
with open('data/SOM'+ "/g_10_a.txt", 'w') as output:
    for row in A_10:
        output.write(str(row) + '\n')

with open('data/SOM'+ "/g_14_a.txt", 'w') as output:
    for row in A_14:
        output.write(str(row) + '\n')

with open('data/SOM'+ "/g_42_a.txt", 'w') as output:
    for row in A_42:
        output.write(str(row) + '\n')


with open('data/SOM'+ "/g_0_b.txt", 'w') as output:
    for row in B_0:
        output.write(str(row) + '\n')
        
with open('data/SOM'+ "/g_10_b.txt", 'w') as output:
    for row in B_10:
        output.write(str(row) + '\n')

with open('data/SOM'+ "/g_14_b.txt", 'w') as output:
    for row in B_14:
        output.write(str(row) + '\n')

with open('data/SOM'+ "/g_42_b.txt", 'w') as output:
    for row in B_42:
        output.write(str(row) + '\n')
         
#%%  
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation

M= np.load('M.py.npy')
m =60
k=0
tol=10
M0 = M[:,0]
M10 = M[:,1]
M14 = M[:,2]
M42 = M[:,3]


UFK = []
tau1 = np.linspace(0,1,50)
tau = tau1[0:20]
tau = np.linspace(0,1,50)
for i in range(len(tau)):
    H = tau[i]*M10+(1-tau[i])*M0
    UFK.append(H.reshape(m,m))
UFK.append(-M10.reshape(m,m))  
UFK.append(-M10.reshape(m,m))    
  
for i in range(len(tau)):
    H = tau[i]*M14+(1-tau[i])*M10
    UFK.append(-H.reshape(m,m))
    
UFK.append(-M14.reshape(m,m))  
UFK.append(-M14.reshape(m,m))    
    
for i in range(len(tau)):
    H = tau[i]*M42+(1-tau[i])*M14
    UFK.append(-H.reshape(m,m))
UFK.append(-M42.reshape(m,m))  
UFK.append(-M42.reshape(m,m))    



def data(t):
    # remove for loop here
    L = UFK[t]
    ax.clear()
    
    
    surf = ax.plot_surface(XX, YY, L, rstride=1, cstride=1, cmap='viridis', linewidth=0, antialiased=False)
    ax.set_zlim([-2,5]) # set zlim to be always the same for every frame


fig = plt.figure()
ax = fig.gca(projection='3d')
#X = np.arange(0, Nx)
#Y = np.arange(0, Ny)
n = UFK[0].shape[0]

XX,YY = np.meshgrid(np.arange(n),np.arange(n))
#np.meshgrid(X, Y)
surf = ax.plot_surface(XX, YY, UFK[0],rstride=1, cstride=1, cmap='viridis', linewidth=0, antialiased=False)
ax.set_zlim(-2, 5)
#ax.zaxis.set_major_locator(LinearLocator(10))
fig.colorbar(surf, shrink=0.5, aspect=10)
ax.set_xlabel('X nodes - Axis')
ax.set_ylabel('Y nodes - Axis')
ax.set_zlabel('Value')

ani = animation.FuncAnimation(fig, data, len(UFK), interval=50, repeat=True )
plt.show()
# Set up formatting for the movie files
fps=10
fn = 'data/plots/plot_surface_animation_funcanimation5'
ani.save(fn+'.mp4',writer='ffmpeg',fps=fps)
ani.save(fn+'.gif',writer='imagemagick',fps=fps)   

#%%
